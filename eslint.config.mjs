// @ts-check

import reactRefresh from "eslint-plugin-react-refresh";

import globals from "globals";
import eslintJs from "@eslint/js";
import tseslint from "typescript-eslint";

import react from "eslint-plugin-react";
import love from "eslint-config-love";
import configPrettier from "eslint-config-prettier";

import importPlugin from "eslint-plugin-import";
import importX from "eslint-plugin-import-x";
import n from "eslint-plugin-n";
import simpleImportSort from "eslint-plugin-simple-import-sort";
import comments from "@eslint-community/eslint-plugin-eslint-comments/configs";
import regexp from "eslint-plugin-regexp";
import security from "eslint-plugin-security";
import tailwind from "eslint-plugin-tailwindcss";
import unicorn from "eslint-plugin-unicorn";
import unusedImports from "eslint-plugin-unused-imports";
import reactHooks from "eslint-plugin-react-hooks";
import promise from "eslint-plugin-promise";
import tsParser from "@typescript-eslint/parser";

const baseFiles = ["**/*.js", "**/*.jsx", "**/*.ts", "**/*.tsx"];

export default tseslint.config(
  { files: ["src/**/*.{js,mjs,cjs,ts,jsx,tsx}"] },
  {
    languageOptions: {
      globals: { ...globals.browser, ...globals.node },
      // これいるのかな？他の設定に含まれているのかも？
      parser: tsParser,
      // https://typescript-eslint.io/getting-started/typed-linting
      parserOptions: {
        projectService: true,
        tsconfigRootDir: import.meta.dirname,
      },
    },
    linterOptions: {
      // noInlineConfig: true,
      reportUnusedDisableDirectives: "error",
    },
  },

  // @eslint/js
  eslintJs.configs.recommended,
  {
    rules: {
      "no-console": "error",
    },
  },

  // typescript-eslint
  ...tseslint.configs.strictTypeChecked,
  ...tseslint.configs.stylisticTypeChecked,

  // plugin-import-x
  importX.flatConfigs.recommended,
  importX.flatConfigs.typescript,
  {
    settings: {},
    rules: {
      "import-x/no-extraneous-dependencies": [
        "error",
        {
          devDependencies: false,
          optionalDependencies: false,
          peerDependencies: false,
        },
      ],
      "import-x/no-relative-packages": "error",
    },
  },

  // plugin-n
  n.configs["flat/recommended"],
  {
    settings: {
      // n/no-missing-import用→よって不要と思われる
      // node: { tryExtensions: ['.js', '.jsx', '.ts', '.tsx', '.d.ts', '/index.ts', '/index.tsx', '/index.d.ts'] },
    },
    rules: {
      "n/no-missing-import": ["off"], // import解決エラーはtsc・bundlerに任せたほうが良い
      "n/no-missing-require": ["off"], // import解決エラーはtsc・bundlerに任せたほうが良い
      "n/no-restricted-import": ["warn", []],
    },
  },

  // plugin-promise
  promise.configs["flat/recommended"],

  // eslint-config-love
  // @typescript-eslint・import・n・promiseのルールセット
  {
    plugins: {
      import: importPlugin,
    },
    rules: love.rules,
  },

  // plugin-unicorn
  unicorn.configs["flat/recommended"],
  {
    rules: {
      "unicorn/filename-case": [
        "warn",
        {
          cases: { pascalCase: true, camelCase: true },
        },
      ], // 現状合わせ
      "unicorn/no-null": ["off"], // prismaで使う
    },
  },
  // plugin-react-hooks
  react.configs.flat.recommended,
  react.configs.flat["jsx-runtime"],
  {
    settings: {
      react: {
        version: "detect",
      },
    },
  },
  // plugin-react-hooks: eslint9未対応
  {
    files: baseFiles,
    plugins: {
      "react-hooks": reactHooks,
    },
    rules: {
      "react-hooks/exhaustive-deps": "error",
      "react-hooks/rules-of-hooks": "error",
    },
  },
  {
    plugins: {
      "react-refresh": reactRefresh,
    },
    rules: {
      "react-refresh/only-export-components": [
        "warn",
        { allowConstantExport: true },
      ],
    },
  },

  // plugin-simple-import-sort
  {
    plugins: {
      "simple-import-sort": simpleImportSort,
    },
    rules: {
      "simple-import-sort/imports": "error",
      "simple-import-sort/exports": "error",
    },
  },

  // plugin-comments https://github.com/mysticatea/eslint-plugin-eslint-comments
  comments.recommended,
  // {
  //   rules: {
  //     '@eslint-community/eslint-comments/disable-enable-pair': ['error', { allowWholeFile: true }],
  //   },
  // },

  // plugin-regexp
  regexp.configs["flat/recommended"],

  // plugin-security
  security.configs.recommended,

  // tailwind
  ...tailwind.configs["flat/recommended"],
  {
    rules: {
      "tailwindcss/no-custom-classname": [
        "error",
        {
          whitelist: [],
        },
      ],
      // 以下はfixable
      "tailwindcss/classnames-order": ["error"],
      "tailwindcss/enforces-shorthand": ["error"],
    },
  },

  // unused-imports
  {
    plugins: {
      "unused-imports": unusedImports,
    },
    rules: {
      "unused-imports/no-unused-imports": "error", // @typescript-eslint/no-unused-varsをerrorにできたらこのプラグインは不要になる
    },
  },

  {
    rules: {},
  },

  {
    rules: {
      "unicorn/no-array-callback-reference": "off",
      "unicorn/no-array-method-this-argument": "off",
      "@typescript-eslint/no-magic-numbers": "warn",
    },
  },

  // eslint対象外
  {
    ignores: [
      "dist/**",
      "vite.config.ts",
      "eslint.config.mjs",
      "setupTests.ts",
    ],
  },

  // config-prettier
  configPrettier,
);
