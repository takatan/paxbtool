import * as RA from "effect/Array";
import { flatten } from "effect/Array";
import { struct } from "effect/Data";
import * as E from "effect/Either";
import { type Either, flatMap, fromNullable, map, right } from "effect/Either";
import { constant, constFalse, flow, pipe } from "effect/Function";
import { get, toEntries } from "effect/HashMap";
import { sumAll } from "effect/Number";
import * as O from "effect/Option";
import * as RR from "effect/Record";
import { filter, filterMap, size, values } from "effect/Record";
import { evolve } from "effect/Struct";

import { areaDataRecord } from "../misc/database";
import {
  addAreaUnit,
  addChangedMarker,
  addMarker,
  type Area,
  getChangedMarker,
  getMarker,
  realIncome,
  removeMarker,
} from "./area";
import {
  getTreasuryTotal,
  getUnit,
  getVPTotal,
  type Homeland,
  modifyMerchant,
  modifyTreasury,
  modifyUnit,
  modifyVP,
} from "./homeland";
import { addMerchant, addSeaUnit, type SeaZone } from "./sea";
import {
  statusData,
  type StatusMarker,
  type StatusShape,
  type StatusType,
  statusTypes,
} from "./status";
import { type Stock, stock } from "./stock";
import {
  type AreaName,
  type PlayerPowerName,
  playerPowerNames,
  type PowerName,
  type SeaName,
  vpDivisor,
} from "./types";
import {
  type AreaUnit,
  unitCost,
  type UnitStrength,
  type UnitType,
} from "./unitType";

// これもできるのじゃないか？l
// type AreaStruct = {
//   [P in AreaName]: Area;
// };
export type AreaRecord = Record<AreaName, Area>;
export type StockRecord = Record<PowerName, Stock>;
export type HomelandRecord = Record<PowerName, Homeland>;
export type SeaZoneRecord = Record<SeaName, SeaZone>;

// VPの計算でドイツとオーストリアは合計する

export interface WorldState {
  areas: AreaRecord;
  seaZones: SeaZoneRecord;
  homelands: HomelandRecord;
  stocks: StockRecord;
}

export type WorldModifier = (w: WorldState) => Either<WorldState, Error>;

//

// export const payment = (amount: number, message: string) => {};

export const getStockCanal: (a: WorldState) => number = flow(
  (x) => x.areas,
  filter((a: Area) => a.canal),
  size,
  (n: number) => 2 - n,
);

const lookupCommon =
  <K extends string, T>(getter: (w: WorldState) => Record<K, T>) =>
  (key: K) =>
  (self: WorldState): Either<T, Error> =>
    pipe(
      self,
      getter,
      (x) => x[key],
      fromNullable(() => new Error(`key = ${key}`)),
    );
// undefinedのgetを呼ぶとエラーが出るが理由がよくわからない

// const modifyCommon00 =
//   <K extends string, T>(f: (a: T) => Either< T,Error>) =>
//   (getter: (w: WorldState) => Record<string, T>) =>
//   (
//     modify: (
//       f: (am: Record<string, T>) => Record<string, T>,
//     ) => (w: WorldState) => WorldState,
//   ) =>
//   (key: K) =>
//   (self: WorldState): Either<WorldState, Error> =>
//     flow(
//       lookupCommon<K, T>(getter)(key),
//       chain(f),
//       map((t) => upsertAt(key, t)),
//       map((g) => modify(g)(self)),
//     )(self);

// Record version: setter & getter
const getCommon =
  <K extends string, T, B>(f: (t: T) => B) =>
  (getter: (w: WorldState) => Record<K, T>) =>
  (key: K) =>
  (self: WorldState): Either<B, Error> =>
    pipe(self, lookupCommon<K, T>(getter)(key), map(f));
export const getSeaZone = <B>(
  f: (s: SeaZone) => B,
): ((key: SeaName) => (w: WorldState) => Either<B, Error>) =>
  getCommon<SeaName, SeaZone, B>(f)((x) => x.seaZones);
export const getHomeland = <B>(
  f: (h: Homeland) => B,
): ((key: PowerName) => (w: WorldState) => Either<B, Error>) =>
  getCommon<PowerName, Homeland, B>(f)((x) => x.homelands);
export const getStock = <B>(
  f: (s: Stock) => B,
): ((key: PowerName) => (w: WorldState) => Either<B, Error>) =>
  getCommon<PowerName, Stock, B>(f)((x) => x.stocks);
export const getArea = <B>(
  f: (key: Area) => B,
): ((s: AreaName) => (w: WorldState) => Either<B, Error>) =>
  getCommon<AreaName, Area, B>(f)((x) => x.areas);

// modifyAtが使えないのは f:(T=>T)ではなくて f:(T=>Either<T,Error>) だから
const modifyCommon =
  <K extends string, T>(f: (a: T) => Either<T, Error>) =>
  (getter: (w: WorldState) => Record<K, T>) =>
  (setter: (am: Record<K, T>) => (w: WorldState) => WorldState) =>
  (key: K) =>
  (self: WorldState): Either<WorldState, Error> =>
    pipe(
      self,
      lookupCommon(getter)(key),
      flatMap(f),
      map((t) => RR.set(key, t)),
      map((g) => setter(g(getter(self)))(self)),
    );
export const modifyArea = (
  f: (a: Area) => Either<Area, Error>,
): ((key: AreaName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyCommon<AreaName, Area>(f)((x) => x.areas)((x) =>
    evolve({ areas: constant(x) }),
  );
export const modifySeaZone = (
  f: (s: SeaZone) => Either<SeaZone, Error>,
): ((key: SeaName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyCommon<SeaName, SeaZone>(f)((x) => x.seaZones)((x) =>
    evolve({ seaZones: constant(x) }),
  );
export const modifyHomeland = (
  f: (h: Homeland) => Either<Homeland, Error>,
): ((key: PowerName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyCommon<PowerName, Homeland>(f)((x) => x.homelands)((x) =>
    evolve({ homelands: constant(x) }),
  );
export const modifyStock = (
  f: (s: Stock) => Either<Stock, Error>,
): ((key: PowerName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyCommon<PowerName, Stock>(f)((x) => x.stocks)((x) =>
    evolve({ stocks: constant(x) }),
  );

// area

export const areaGetMarker = flow(getMarker, getArea);

export const areaGetChangedMarker = flow(getChangedMarker, getArea);

export const areaSetUnrest = (
  value: boolean,
): ((a: AreaName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyArea(flow(evolve({ unrest: constant(value) }), right));

// homeland

export const homelandGetMoney = getHomeland(getTreasuryTotal);

export const homelandGetMerchant = getHomeland((x) => x.merchant);

export const homelandGetUnit = (
  type: UnitType,
  strength: UnitStrength,
): ((a: PowerName) => (w: WorldState) => Either<number, Error>) =>
  getHomeland(getUnit(struct({ type, strength })));

export const homelandModifyTreasury = (
  diff: number,
): ((key: PowerName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyHomeland(modifyTreasury(diff));

export const homelandTreasuryTotal = getHomeland(getTreasuryTotal);

export const homelandGetVP =
  (powerName: PlayerPowerName) =>
  (self: WorldState): Either<number, unknown> => {
    const s =
      powerName === "Germany"
        ? right((a: number) => (b: number) => a + b).pipe(
            E.ap(homelandTreasuryTotal(powerName)(self)),
            E.ap(homelandTreasuryTotal("Austria-Hungary")(self)),
          )
        : homelandTreasuryTotal(powerName)(self);
    return right(
      (a: number) => (b: number) => a + Math.floor(b / vpDivisor[powerName]),
    ).pipe(E.ap(getHomeland(getVPTotal)(powerName)(self)), E.ap(s));
  };
// stock

export const stockGetMerchant = getStock((x) => x.merchant);

export const stockGetStatusMarker = (
  shape: StatusShape,
): ((a: PowerName) => (w: WorldState) => Either<number, Error>) =>
  getStock(stock.getStatusMarker(shape));

export const stockGetUnit = (
  strength: UnitStrength,
): ((a: PowerName) => (w: WorldState) => Either<number, Error>) =>
  getStock(stock.getUnit(strength));

export const stockModifyUnit = (
  strength: UnitStrength,
  diff: number,
): ((a: PowerName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyStock(stock.modifyUnit(strength, diff));

export const stockModifyStatusMarker = (
  m: StatusShape,
  diff: number,
): ((a: PowerName) => (w: WorldState) => Either<WorldState, Error>) =>
  modifyStock(stock.modifyStatusMarker(m, diff));

/** Stockから移動 */
export const moveMarkerStockToArea = (
  power: PowerName,
  areaName: AreaName,
  status: StatusMarker,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    stockModifyStatusMarker(status.shape, -1)(power),
    flatMap(modifyArea(addMarker(power, status))(areaName)),
  );
export const moveMerchantStockToSea = (
  power: PowerName,
  seaName: SeaName,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    modifyStock(stock.modifyMerchant(-1))(power),
    flatMap(modifySeaZone((s) => addMerchant(power)(s))(seaName)),
  );
export const moveUnitStockToArea = (
  areaUnit: AreaUnit,
  areaName: AreaName,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    stockModifyUnit(areaUnit.strength, -1)(areaUnit.power),
    flatMap(modifyArea(flow(addAreaUnit(areaUnit), right))(areaName)),
  );
export const moveUnitStockToSea = (
  areaUnit: AreaUnit,
  seaName: SeaName,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    stockModifyUnit(areaUnit.strength, -1)(areaUnit.power),
    flatMap(modifySeaZone(flow(addSeaUnit(areaUnit), right))(seaName)),
  );

// TODO 引数の型をAreaUnitで全部処理するか考える #40
export const moveUnitStockToHome = (
  power: PowerName,
  type: UnitType,
  strength: UnitStrength,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    stockModifyUnit(strength, -1)(power),
    flatMap(modifyHomeland(modifyUnit(struct({ type, strength }), 1))(power)),
  );
// 必要なのか検討
export const moveMerchantStockToHome = (
  power: PowerName,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    modifyStock(stock.modifyMerchant(-1))(power),
    flatMap(modifyHomeland(modifyMerchant(1))(power)),
  );

export const stockNonEmpty =
  (shape: StatusShape) =>
  (powerName: PowerName): ((s: WorldState) => boolean) =>
    flow(
      stockGetStatusMarker(shape)(powerName),
      map((x) => x > 0),
      E.getOrElse(constant(false)),
    );

//

export const upgradeMarker = (
  power: PowerName,
  areaName: AreaName,
  oldMarker: StatusMarker,
  newMarker: StatusMarker,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    modifyArea(removeMarker(power, oldMarker))(areaName),
    flatMap(stockModifyStatusMarker(oldMarker.shape, 1)(power)),
    flatMap(stockModifyStatusMarker(newMarker.shape, -1)(power)),
    flatMap(modifyArea(addChangedMarker(power, newMarker))(areaName)),
  );

export const purchaseHomeUnit = (
  power: PowerName,
  unit: UnitType,
  strength: UnitStrength,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    homelandModifyTreasury(-unitCost(strength))(power),
    flatMap(moveUnitStockToHome(power, unit, strength)),
  );

export const purchaseChangedMarker = (
  power: PowerName,
  areaName: AreaName,
  status: StatusMarker,
): ((w: WorldState) => Either<WorldState, Error>) =>
  flow(
    homelandModifyTreasury(-statusData[status.type].price)(power),
    flatMap(stockModifyStatusMarker(status.shape, -1)(power)),
    flatMap(modifyArea(addChangedMarker(power, status))(areaName)),
  );

//

export const enoughMoney =
  (price: number) =>
  (powerName: PowerName): ((w: WorldState) => boolean) =>
    flow(
      homelandGetMoney(powerName),
      map((n) => n - price > 0),
      E.getOrElse(constFalse),
    );

// 集計表示
export const sumOfMarker =
  (status: StatusType) =>
  (power: PowerName) =>
  (self: WorldState): number =>
    pipe(
      self.areas,
      filterMap((a: Area) => get(a.markers, power)),
      values,
      RA.filter((s: StatusMarker) => s.type === status),
      RA.length,
    );
export const sumOfValue =
  (status: StatusType) =>
  (power: PowerName) =>
  (self: WorldState): number =>
    pipe(
      self.areas,
      RR.filterMap((a: Area, k: AreaName) =>
        O.map((s: StatusMarker) =>
          s.type === status ? realIncome(areaDataRecord[k].ev)(a) : 0,
        )(get(a.markers, power)),
      ),
      RR.values,
      sumAll,
    );
export const sumOfUnitCost =
  (power: PowerName) =>
  (self: WorldState): number =>
    pipe(
      RR.filter(
        self.areas,
        (a: Area) =>
          !pipe(
            get(a.markers, power),
            O.exists(
              (marker: StatusMarker) =>
                marker.type === "Dominion" || marker.type === "State",
            ),
          ),
      ),
      RR.values,
      RA.map((a: Area) => toEntries(a.units)),
      flatten,
      RA.filter((x: [AreaUnit, number]) => x[0].power === power),
      RA.map((x: [AreaUnit, number]) => x[0].strength * x[1]),
      sumAll,
    );
export const sumOfCostTotal =
  (status: StatusType) =>
  (power: PowerName) =>
  (self: WorldState): number =>
    sumOfMarker(status)(power)(self) * statusData[status].cost;
export const totalCost =
  (power: PowerName) =>
  (self: WorldState): number =>
    pipe(
      statusTypes,
      RA.map((type: StatusType) => sumOfCostTotal(type)(power)(self)),
      sumAll,
    ) + sumOfUnitCost(power)(self);
export const sumOfValueTotal =
  (status: StatusType) =>
  (power: PowerName) =>
  (self: WorldState): number =>
    sumOfValue(status)(power)(self) * statusData[status].income;
export const totalIncome =
  (power: PowerName) =>
  (self: WorldState): number =>
    pipe(
      statusTypes,
      RA.map((type: StatusType) => sumOfValueTotal(type)(power)(self)),
      sumAll,
    );
export const totalBalance =
  (power: PowerName) =>
  (self: WorldState): number =>
    totalIncome(power)(self) - totalCost(power)(self);

export const addFinalVP = (w: WorldState): Either<WorldState, Error> =>
  pipe(
    RA.map((p: PlayerPowerName) =>
      flatMap(
        modifyHomeland(
          flow((h: Homeland) => {
            const income =
              (p === "Germany" ? totalIncome("Austria-Hungary")(w) : 0) +
              totalIncome(p)(w);
            return modifyVP(
              Math.floor((income * 2) / vpDivisor[p]),
              `最終VP(${income}*2/${vpDivisor[p]})`,
            )(h);
          }, right),
        )(p),
      ),
    )(playerPowerNames),
    RA.reduce(right(w), (b: E.Either<WorldState, Error>, a) => a(b)),
  );
