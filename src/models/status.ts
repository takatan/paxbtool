// round => Interest | Influence
// hex => Protectorate | Possession
// dominion => Possession | Dominion
// state => Possession | State

export const statusTypes = [
  "Interest",
  "Influence",
  "Protectorate",
  "Possession",
  "Dominion",
  "State",
] as const;

export type StatusType = (typeof statusTypes)[number];

export const statusShapes = ["round", "hex", "dominion", "state"] as const;
export type StatusShape = (typeof statusShapes)[number];

interface IStatusMarker {
  shape: StatusShape;
  type: StatusType;
}

export const interestMarker: IStatusMarker = {
  shape: "round",
  type: "Interest",
};

export const influenceMarker: IStatusMarker = {
  shape: "round",
  type: "Influence",
};
export const protectorateMarker: IStatusMarker = {
  shape: "hex",
  type: "Protectorate",
};
export const possessionMarker: IStatusMarker = {
  shape: "hex",
  type: "Possession",
};

export const dominionMarker: IStatusMarker = {
  shape: "dominion",
  type: "Dominion",
};

export const stateMarker: IStatusMarker = {
  shape: "state",
  type: "State",
};

export const statusMarkers = [
  interestMarker,
  influenceMarker,
  protectorateMarker,
  possessionMarker,
  dominionMarker,
  stateMarker,
] as const;

export type StatusMarker = (typeof statusMarkers)[number];
interface IStatusEntity {
  level: number;
  income: number;
  price: number;
  cost: number;
  text: string;
  defaultMarker: StatusMarker;
}
export const statusData: Record<StatusType, IStatusEntity> = {
  Interest: {
    level: 1,
    text: "利権",
    income: 1,
    price: 5,
    cost: 0,
    defaultMarker: interestMarker,
  },
  Influence: {
    level: 2,
    text: "影響下",
    income: 2,
    price: 10,
    cost: 5,
    defaultMarker: influenceMarker,
  },
  Protectorate: {
    level: 3,
    text: "保護領",
    income: 4,
    price: 20,
    cost: 10,
    defaultMarker: protectorateMarker,
  },
  Possession: {
    level: 4,
    text: "占領",
    income: 5,
    price: 40,
    cost: 20,
    defaultMarker: possessionMarker,
  },
  Dominion: {
    level: 5,
    text: "自治領",
    income: 5,
    price: 60,
    cost: 30,
    defaultMarker: dominionMarker,
  },
  State: {
    level: 5,
    text: "州",
    income: 5,
    price: 60,
    cost: 30,
    defaultMarker: stateMarker,
  },
};
