import { type Either, filterOrLeft, right } from "effect/Either";
import { flow, pipe } from "effect/Function";
import { format } from "effect/Inspectable";
import { evolve } from "effect/Struct";

import type { StatusShape } from "./status";
import type { UnitStrength } from "./unitType";

export type StatusKeys = Record<StatusShape, number>;

export interface Stock extends StatusKeys {
  merchant: number;
  units: {
    10: number;
    5: number;
    3: number;
    1: number;
  };
}

const getUnit =
  (strength: UnitStrength) =>
  (self: Stock): number =>
    self.units[strength];

const getStatusMarker =
  (shape: StatusShape) =>
  (self: Stock): number =>
    self[shape];

const modifyMerchant = (diff: number): ((s: Stock) => Either<Stock, Error>) =>
  flow(
    evolve({
      merchant: (m) => m + diff,
    }),
    right,
    filterOrLeft(
      (s: Stock) => s.merchant >= 0,
      (s) => new Error(`modifyMerchant: ${JSON.stringify(s)}`),
    ),
  );

const modifyStatusMarker = (
  m: StatusShape,
  diff: number,
): ((s: Stock) => Either<Stock, Error>) =>
  flow(
    (s: Stock) => ({ ...s, [m]: s[m] + diff }),
    // evolve({ [m]: (n: number) => n + diff }), // 型推論がうまくいかない
    right,
    filterOrLeft(
      (s: Stock) => s[m] >= 0,
      (s) => new Error(`modifyStatusMarker: ${JSON.stringify(s)}`),
    ),
  );

const modifyUnit = (
  strength: UnitStrength,
  diff: number,
): ((self: Stock) => Either<Stock, Error>) =>
  flow(
    evolve({
      units: (a) => pipe(a, evolve({ [strength]: (n: number) => n + diff })),
    }),
    right,
    filterOrLeft(
      (s: Stock) => getUnit(strength)(s) >= 0,
      (s) => new Error(`modifyUnit: ${strength} ${diff} ${format(s)}`),
    ),
  );

export const stock = {
  getUnit,
  getStatusMarker,
  modifyUnit,
  modifyMerchant,
  modifyStatusMarker,
};
