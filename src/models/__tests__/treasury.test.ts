import { describe, expect, test } from "vitest";

import { treasuryTotal } from "../treasury";

describe("Treasury", () => {
  test("empty", () => {
    expect(treasuryTotal([{ amount: 10, reason: "" }])).toBe(10);
  });
});
