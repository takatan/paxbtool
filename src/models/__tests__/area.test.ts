import { struct } from "effect/Data";
import { flatMap, isLeft, isRight, map, right } from "effect/Either";
import { constant, flow } from "effect/Function";
import { get, size } from "effect/HashMap";
import { getOrElse } from "effect/Option";
import { describe, expect, test } from "vitest";

import { defaultArea } from "../../store/default";
import {
  addAreaUnit,
  addChangedMarker,
  addMarker,
  isCasusBelli,
  realIncome,
  removeMarker,
  setCanal,
} from "../area";
import { influenceMarker, interestMarker } from "../status";
import type { AreaUnit } from "../unitType";

describe("Area", () => {
  const GreatBritain = "Great Britain";
  const Germany = "Germany";

  describe("realIncome", () => {
    test("共同統治なし", () => {
      const b = addMarker(GreatBritain, { shape: "hex", type: "Possession" })(
        defaultArea,
      );
      expect(map(realIncome(10))(b)).toStrictEqual(right(10));
    });

    test("2国共同統治", () => {
      const a = flow(
        addMarker(GreatBritain, {
          shape: "hex",
          type: "Possession",
        }),
        flatMap(addMarker(Germany, { shape: "hex", type: "Possession" })),
      )(defaultArea);
      expect(map(realIncome(10))(a)).toStrictEqual(right(9));
    });
  });
  describe("AreaUnit", () => {
    // test("eq", () => {
    //   const a: AreaUnit = {
    //     power: "France",
    //     type: "Army",
    //     strength: 1,
    //   };
    //   const b: AreaUnit = {
    //     power: "France",
    //     type: "Army",
    //     strength: 1,
    //   };
    //   expect(eqAreaUnit.equals(a, b)).toBeTruthy();
    // });
    test("addSeaUnit", () => {
      const k: AreaUnit = struct({
        power: GreatBritain,
        type: "Army",
        strength: 1,
      });
      const b = addAreaUnit(k)(defaultArea);
      expect(size(b.units)).toStrictEqual(1);
      expect(getOrElse(constant(0))(get(k)(b.units))).toStrictEqual(1);
    });
  });
  test("add & remove Marker", () => {
    const a = addMarker(GreatBritain, interestMarker)(defaultArea);
    expect(
      isRight(flatMap(removeMarker(GreatBritain, interestMarker))(a)),
    ).toBeTruthy();
    expect(
      isLeft(flatMap(removeMarker(GreatBritain, influenceMarker))(a)),
    ).toBeTruthy();
  });
  test("addChangedMarker", () => {
    const a = addMarker(GreatBritain, interestMarker)(defaultArea);
    expect(
      isRight(addChangedMarker(GreatBritain, influenceMarker)(defaultArea)),
    ).toBeTruthy();
    expect(
      isLeft(flatMap(addChangedMarker(GreatBritain, interestMarker))(a)),
    ).toBeTruthy();
  });
  test("casus belli", () => {
    const a = flow(
      addMarker(GreatBritain, {
        shape: "hex",
        type: "Possession",
      }),
    )(defaultArea);
    expect(map(isCasusBelli)(a)).toStrictEqual(right(false));
  });
  test("canal", () => {
    const a = addMarker(GreatBritain, influenceMarker)(defaultArea);
    const b = flatMap(setCanal(true, GreatBritain))(a);

    expect(map(b, (a) => a.canal)).toStrictEqual(right(true));

    expect(isRight(flatMap(setCanal(true, GreatBritain))(a))).toBeTruthy();
    expect(isRight(flatMap(setCanal(false, GreatBritain))(b))).toBeTruthy();

    expect(isLeft(flatMap(setCanal(true, GreatBritain))(b))).toBeTruthy();
    expect(isLeft(flatMap(setCanal(false, GreatBritain))(a))).toBeTruthy();
  });
});
