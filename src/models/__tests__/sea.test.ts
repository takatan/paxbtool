import { flatMap, isLeft, map, right } from "effect/Either";
import { size } from "effect/HashSet";
import { expect, test } from "vitest";

import { defaultSeaZone } from "../../store/default";
import { addMerchant, removeMerchant, type SeaZone } from "../sea";

test("addMerchant", () => {
  const s = defaultSeaZone;
  const GreatBritain = "Great Britain";
  const France = "France";

  expect(size(s.merchants)).toBe(0);
  const s1 = addMerchant(GreatBritain)(s);
  expect(map((a: SeaZone) => size(a.merchants))(s1)).toStrictEqual(right(1));
  expect(isLeft(flatMap(addMerchant(GreatBritain))(s1))).toBeTruthy();
  expect(isLeft(flatMap(removeMerchant(France))(s1))).toBeTruthy();
});
