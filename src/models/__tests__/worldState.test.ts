import { Equal } from "effect";
import { struct } from "effect/Data";
import { flatMap, getOrThrow, isLeft, isRight, right } from "effect/Either";
import { flow, pipe } from "effect/Function";
import * as HM from "effect/HashMap";
import { fromIterable } from "effect/HashSet";
import { none, some } from "effect/Option";
import { describe, expect, test } from "vitest";

import { emptyWorldState, initWorldState } from "../../store/default";
import { startWorldState } from "../../store/startpos";
import type { StatusMarker } from "../status";
import type { AreaName, SeaName } from "../types";
import type { AreaUnit } from "../unitType";
import {
  areaGetChangedMarker,
  areaGetMarker,
  enoughMoney,
  getArea,
  getSeaZone,
  homelandGetMerchant,
  homelandGetMoney,
  homelandGetUnit,
  homelandModifyTreasury,
  moveMarkerStockToArea,
  moveMerchantStockToHome,
  moveMerchantStockToSea,
  moveUnitStockToArea,
  purchaseChangedMarker,
  purchaseHomeUnit,
  stockNonEmpty,
} from "../worldState";

describe("WorldState", () => {
  const GreatBritain = "Great Britain";
  const France = "France";
  const Germany = "Germany";
  const China = "Chinese Empire";
  const Spain = "Spain";
  const AustriaHungary = "Austria-Hungary";
  const Interest: StatusMarker = { shape: "round", type: "Interest" };
  const Influence: StatusMarker = { shape: "round", type: "Influence" };
  const Army = "Army";
  // const Fleet = "Fleet";
  describe("WorldState の default", () => {
    test("空の状態でplaceMarkerの失敗", () => {
      const r = purchaseChangedMarker(
        GreatBritain,
        "Egypt",
        Interest,
      )(emptyWorldState);
      expect(isLeft(r)).toBeTruthy();
    });
  });
  describe("initWorldState()", () => {
    const w = initWorldState();
    const egypt = "Egypt";
    describe("状態マーカー配置", () => {
      test("空白地に行う", () => {
        const r = moveMarkerStockToArea(GreatBritain, egypt, Interest)(w);
        expect(flatMap(areaGetMarker(GreatBritain)(egypt))(r)).toStrictEqual(
          right(some(Interest)),
        );
      });
      test("二人の国が置く", () => {
        const r = flatMap(moveMarkerStockToArea(France, egypt, Interest))(
          moveMarkerStockToArea(GreatBritain, egypt, Interest)(w),
        );
        expect(flatMap(areaGetMarker(GreatBritain)(egypt))(r)).toStrictEqual(
          right(some(Interest)),
        );
        expect(flatMap(areaGetMarker(France)(egypt))(r)).toStrictEqual(
          right(some(Interest)),
        );
      });
      test("同一国が二つ置くとエラー", () => {
        const r = flatMap(
          moveMarkerStockToArea(GreatBritain, egypt, Influence),
        )(moveMarkerStockToArea(GreatBritain, egypt, Interest)(w));
        expect(isLeft(r)).toBeTruthy();
      });
      test("Interestマーカーがstockが空でエラー", () => {
        const r = moveMarkerStockToArea(China, egypt, Interest)(w);
        expect(isLeft(r)).toBeTruthy();
      });
      test("stockEmpty", () => {
        expect(!stockNonEmpty("round")(China)(w)).toBeTruthy();
        expect(!stockNonEmpty("round")(GreatBritain)(w)).toBeFalsy();
      });
      test("notEnoughMoney", () => {
        expect(!enoughMoney(100)(China)(w)).toBeTruthy();
        expect(!enoughMoney(1)(GreatBritain)(startWorldState)).toBeFalsy();
      });
    });
    describe("状態変更マーカー配置", () => {
      describe("空白地に行う", () => {
        const r = flow(
          homelandModifyTreasury(10)(GreatBritain),
          flatMap(purchaseChangedMarker(GreatBritain, egypt, Interest)),
        )(w);
        test("markerがundefinedのまま", () => {
          expect(flatMap(areaGetMarker(GreatBritain)(egypt))(r)).toStrictEqual(
            right(none()),
          );
        });
        test("changed_markerにINTEREST", () => {
          expect(
            flatMap(areaGetChangedMarker(GreatBritain)(egypt))(r),
          ).toStrictEqual(right(some(Interest)));
        });
        test("所持金が減る", () => {
          expect(flatMap(homelandGetMoney(GreatBritain))(r)).toStrictEqual(
            right(5),
          );
        });
      });
      test("自分の状態マーカー置いてあるとエラー", () => {
        const r = flow(
          homelandModifyTreasury(10)(GreatBritain),
          flatMap(
            moveMarkerStockToArea(GreatBritain, egypt, {
              shape: "hex",
              type: "Protectorate",
            }),
          ),
          flatMap(purchaseChangedMarker(GreatBritain, egypt, Interest)),
        )(w);
        expect(isLeft(r)).toBeTruthy();
      });
      test("自分の変更マーカー置いてあるとエラー", () => {
        const r = flow(
          homelandModifyTreasury(10)(GreatBritain),
          flatMap(purchaseChangedMarker(GreatBritain, egypt, Interest)),
          flatMap(purchaseChangedMarker(GreatBritain, egypt, Influence)),
        )(w);
        expect(isLeft(r)).toBeTruthy();
      });
      test("他国の状態マーカー・変更マーカーがあっても置ける", () => {
        const r = flow(
          homelandModifyTreasury(10)(GreatBritain),
          flatMap(homelandModifyTreasury(10)(France)),
          flatMap(purchaseChangedMarker(France, egypt, Interest)),
          flatMap(moveMarkerStockToArea(Germany, egypt, Interest)),
          flatMap(purchaseChangedMarker(GreatBritain, egypt, Influence)),
        )(w);
        expect(
          flatMap(areaGetChangedMarker(GreatBritain)(egypt))(r),
        ).toStrictEqual(right(some(Influence)));
      });
      test("所持金不足で購入がエラー", () => {
        const r = purchaseChangedMarker(GreatBritain, egypt, Interest)(w);
        expect(isLeft(r)).toBeTruthy();
      });
    });
    describe("軍隊", () => {
      const p = GreatBritain;
      const a0: AreaName = "Guiana";
      // const a1: AreaName = "Egypt";
      test("初期の軍隊は0", () => {
        expect(homelandGetUnit(Army, 1)(p)(w)).toStrictEqual(right(0));
      });
      test("陸軍1を購入", () => {
        const h1 = pipe(
          homelandModifyTreasury(10)(p)(w),
          flatMap(purchaseHomeUnit(p, Army, 1)),
        );
        expect(flatMap(homelandGetMoney(p))(h1)).toStrictEqual(right(7));
        expect(flatMap(homelandGetUnit(Army, 1)(p))(h1)).toStrictEqual(
          right(1),
        );
      });
      test("資金不足で購入できない。", () => {
        const h1 = flatMap(purchaseHomeUnit(p, Army, 3))(
          homelandModifyTreasury(7)(p)(w),
        );
        expect(homelandGetUnit(Army, 1)(p)(w)).toStrictEqual(right(0));
        expect(isLeft(flatMap(homelandGetMoney(p))(h1))).toBeTruthy();
      });
      test("陸軍を配置", () => {
        const u: AreaUnit = struct({ power: p, type: Army, strength: 1 });
        const w1 = moveUnitStockToArea(u, a0)(w);
        const modified = getOrThrow(
          pipe(w1, flatMap(getArea((a) => a.units)(a0))),
        );
        expect(Equal.equals(modified, HM.fromIterable([[u, 1]]))).toBeTruthy();
      });
    });
    describe("商船", () => {
      const baltic: SeaName = "Baltic Sea";
      // const black: SeaName = "Black Sea";
      test("初期の商船は0", () => {
        expect(homelandGetMerchant(Germany)(w)).toStrictEqual(right(0));
      });
      test("商船を1作成", () => {
        // const sm = w.stockGetMerchant(GreatBritain);
        expect(
          flatMap(homelandGetMerchant(GreatBritain))(
            moveMerchantStockToHome(GreatBritain)(w),
          ),
        ).toStrictEqual(right(1));
      });
      test("商船の在庫なしはエラー", () => {
        expect(isLeft(moveMerchantStockToHome(AustriaHungary)(w))).toBeTruthy();
      });
      test("商船の配置", () => {
        const w1 = moveMerchantStockToSea(GreatBritain, baltic)(w);
        const modified = getOrThrow(
          flatMap(getSeaZone((x) => x.merchants)(baltic))(w1),
        );

        expect(
          Equal.equals(modified, fromIterable([GreatBritain])),
        ).toBeTruthy();
      });
    });
    describe("所持金", () => {
      test("初期の所持金は0", () => {
        expect(homelandGetMoney(Germany)(w)).toStrictEqual(right(0));
      });
      test("所持金をマイナスにできない", () => {
        const r = homelandModifyTreasury(-10)(Spain)(w);
        expect(isLeft(r)).toBeTruthy();
      });
    });
    describe("Areaデータの確認", () => {
      test("Guianaの存在", () => {
        expect(isRight(areaGetMarker(France)("Guiana")(w))).toBeTruthy();
      });
    });
  });
});
