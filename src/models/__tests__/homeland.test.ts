import { struct } from "effect/Data";
import {
  flatMap,
  getOrThrow,
  isLeft,
  left,
  map,
  mapLeft,
  right,
} from "effect/Either";
import { describe, expect, it, test } from "vitest";

import { defaultHomeland } from "../../store/default";
import {
  getTreasuryTotal,
  getUnit,
  modifyTreasury,
  modifyUnit,
} from "../homeland";
import type { UnitCounter } from "../unitType";

describe("Homeland", () => {
  const h = defaultHomeland;

  test("homelandModifyTreasury", () => {
    const h1 = modifyTreasury(10)(h);
    expect(map(getTreasuryTotal)(h1)).toStrictEqual(right(10));

    const h2 = flatMap(modifyTreasury(-5))(h1);
    expect(map(getTreasuryTotal)(h2)).toStrictEqual(right(5));

    const h3 = flatMap(modifyTreasury(-10))(h2);
    expect(isLeft(h3)).toBeTruthy();
    expect(mapLeft((l: Error) => l.message)(h3)).toStrictEqual(
      left("negative"),
    );
  });
  it("modifyUnit", () => {
    const uc: UnitCounter = struct({ type: "Army", strength: 1 });
    const modified = getOrThrow(modifyUnit(uc, 1)(h));
    expect(getUnit(uc)(modified)).toEqual(1);
  });
  describe("getUnit", () => {
    it("default", () => {
      const uc: UnitCounter = struct({ type: "Army", strength: 1 });
      expect(getUnit(uc)(defaultHomeland)).toEqual(0);
    });
    it("1", () => {
      const uc: UnitCounter = struct({ type: "Army", strength: 1 });
      const uc2: UnitCounter = struct({ ...uc });
      const h = modifyUnit(uc, 1)(defaultHomeland);
      expect(map(h, getUnit(uc))).toEqual(right(1));
      expect(map(h, getUnit(uc2))).toEqual(right(1));
    });
  });

  // it("hashmap", () => {
  //   const a: UnitCounter = struct({ type: "Army", strength: 1 });
  //   const b: UnitCounter = struct({ type: "Army", strength: 1 });
  //   const hm: HashMap<UnitCounter, number> = fromIterable([[a, 1]]);
  //   expect(get(a)(hm)).toEqual(some(1));
  //   expect(get(b)(hm)).toEqual(some(1));
  // });
});
