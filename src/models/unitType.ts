import { struct } from "effect/Data";
import * as Order from "effect/Order";

import type { PowerName } from "./types";

export const unitTypes = ["Army", "Fleet"] as const;
export type UnitType = (typeof unitTypes)[number];

export const unitStrengths = [1, 3, 5, 10] as const;
export type UnitStrength = (typeof unitStrengths)[number];

export const unitCost = (s: UnitStrength): number => {
  switch (s) {
    case 1: {
      return 3;
    }
    case 3: {
      return 10;
    }
    case 5: {
      return 0;
    }
    case 10: {
      return 30;
    }
    default: {
      throw new Error(s satisfies never);
    }
  }
};

export const unitPrice = (s: UnitStrength): number => {
  switch (s) {
    case 1: {
      return 3;
    }
    case 3: {
      return 10;
    }
    case 5: {
      return 0;
    }
    case 10: {
      return 30;
    }
    default: {
      throw new Error(s satisfies never);
    }
  }
};

export interface UnitCounter {
  type: UnitType;
  strength: UnitStrength;
}
Order.struct({
  type: Order.string,
  strength: Order.number,
});
export type UnitAbbr = "A1" | "A3" | "A10" | "F1" | "F3" | "F10";

export const unitAbbrToUnitCounter = (unitAbbr: UnitAbbr): UnitCounter => {
  const Army = "Army";
  const Fleet = "Fleet";
  switch (unitAbbr) {
    case "A1": {
      return struct({ type: Army, strength: 1 });
    }
    case "A3": {
      return struct({ type: Army, strength: 3 });
    }
    case "A10": {
      return struct({ type: Army, strength: 10 });
    }
    case "F1": {
      return struct({ type: Fleet, strength: 1 });
    }
    case "F3": {
      return struct({ type: Fleet, strength: 3 });
    }
    case "F10": {
      return struct({ type: Fleet, strength: 10 });
    }
    default: {
      throw new Error(unitAbbr satisfies never);
    }
  }
};

export interface AreaUnit {
  power: PowerName;
  type: UnitType;
  strength: UnitStrength;
}
