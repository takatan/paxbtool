import { append, map } from "effect/Array";
import { type Either, filterOrLeft, right } from "effect/Either";
import { constant, flow, pipe } from "effect/Function";
import { greaterThanOrEqualTo, sumAll } from "effect/Number";

interface ITreasuryEvent {
  amount: number;
  reason: string;
}
export type TreasuryEvent = ITreasuryEvent;
export type TreasuryEvents = TreasuryEvent[];

export const treasuryTotal = (events: TreasuryEvent[]): number =>
  flow(
    map((e: TreasuryEvent) => e.amount),
    sumAll,
  )(events);

export const addTreasuryEvent =
  (event: TreasuryEvent) =>
  (events: TreasuryEvents): Either<TreasuryEvents, Error> =>
    // flowに入れると型推論ができない
    pipe(
      events,
      append(event),
      right,
      filterOrLeft(
        flow(treasuryTotal, greaterThanOrEqualTo(0)),
        constant(new Error(`negative`)),
      ),
    );
