import { append, map } from "effect/Array";
import { flow } from "effect/Function";
import { sumAll } from "effect/Number";

interface IVPEvent {
  vp: number;
  reason: string;
}
export type VPEvent = IVPEvent;
export type VPEvents = VPEvent[];

// TODO: 不要かも
export const addVPEvent = (event: VPEvent): ((events: VPEvents) => VPEvents) =>
  append(event);

// TODO: 不要かも
export const vpTotal = (events: VPEvent[]): number =>
  flow(
    map((e: VPEvent) => e.vp),
    sumAll,
  )(events);
