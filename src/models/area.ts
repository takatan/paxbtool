import { type Either, filterOrLeft, map, right } from "effect/Either";
import { constant, flow, pipe } from "effect/Function";
import * as HM from "effect/HashMap";
import {
  filter,
  get,
  has,
  type HashMap,
  isEmpty,
  modify,
  modifyAt,
  remove,
  set,
} from "effect/HashMap";
import { format } from "effect/Inspectable";
import * as O from "effect/Option";
import {
  contains,
  exists,
  isSome,
  match,
  type Option,
  some,
} from "effect/Option";
import { evolve } from "effect/Struct";

import {
  dominionMarker,
  influenceMarker,
  possessionMarker,
  protectorateMarker,
  stateMarker,
  statusData,
  type StatusMarker,
} from "./status";
import type { PowerName } from "./types";
import type { AreaUnit } from "./unitType";

export interface Area {
  markers: HashMap<PowerName, StatusMarker>;
  changedMarkers: HashMap<PowerName, StatusMarker>;
  units: HashMap<AreaUnit, number>;
  unrest: boolean;
  canal: boolean;
}

export const getMarker =
  (power: PowerName) =>
  (self: Area): Option<StatusMarker> =>
    HM.get(power)(self.markers);

export const getChangedMarker =
  (power: PowerName) =>
  (self: Area): Option<StatusMarker> =>
    HM.get(power)(self.changedMarkers);

export const realIncome =
  (value: number) =>
  (self: Area): number =>
    value -
    Math.max(
      pipe(
        self.markers,
        HM.filter(
          (v: StatusMarker) =>
            v.type === "Protectorate" || v.type === "Possession",
        ),
        HM.size,
      ) - 1,
      0,
    );

// TODO エラーメッセージがいまいち。順番にチェックできる形にできない?
export const addMarker = (
  power: PowerName,
  status: StatusMarker,
): ((a: Area) => Either<Area, Error>) =>
  flow(
    right,
    filterOrLeft(
      (a: Area) => !(has(power)(a.changedMarkers) || has(power)(a.markers)),
      (a: Area) =>
        new Error(
          `${power}のマーカーが既にあります。${format(a.markers)} ${format(a.changedMarkers)}`,
        ),
    ),
    map(
      evolve({
        markers: set(power, status),
      }),
    ),
  );

export const removeMarker = (
  power: PowerName,
  statusMarker: StatusMarker,
): ((a: Area) => Either<Area, Error>) =>
  flow(
    right,
    filterOrLeft(
      (a: Area) => pipe(a.markers, get(power), contains(statusMarker)),
      () => new Error(`${power}->${format(statusMarker)}がない`),
    ),
    map(evolve({ markers: (x) => remove(power)(x) })),
  );

export const addChangedMarker = (
  power: PowerName,
  status: StatusMarker,
): ((a: Area) => Either<Area, Error>) =>
  flow(
    right,
    filterOrLeft(
      (a: Area) => !(has(power)(a.changedMarkers) || has(power)(a.markers)),
      () => new Error(`${power}のマーカーが既にあります`),
    ),
    map(evolve({ changedMarkers: set(power, status) })),
  );
export const removeChangedMarker = (
  power: PowerName,
  statusMarker: StatusMarker,
): ((a: Area) => Either<Area, Error>) =>
  flow(
    right,
    filterOrLeft(
      (a: Area) => pipe(a.changedMarkers, get(power), contains(statusMarker)),
      () => new Error(`${power}->${format(statusMarker)}がない`),
    ),
    map(
      evolve({
        changedMarkers: (x) => remove(power)(x),
      }),
    ),
  );

export const addAreaUnit = (areaUnit: AreaUnit): ((a: Area) => Area) =>
  evolve({
    units: modifyAt(
      areaUnit,
      match({
        onSome: (n: number) => some(n + 1),
        onNone: constant(some(1)),
      }),
    ),
  });

export const removeAreaUnit = (
  areaUnit: AreaUnit,
): ((self: Area) => Either<Area, Error>) =>
  flow(
    right,
    filterOrLeft(
      flow(
        (x) => x.units,
        get(areaUnit),
        O.filter((n) => n >= 1),
        isSome,
      ),
      constant(new Error("removeAreaUnit: unit count should be >= 1")),
    ),
    map(
      evolve({
        units: (units) =>
          pipe(
            units,
            modify(areaUnit, (n) => n - 1),
          ),
      }),
    ),
  );

export const setCanal = (
  flag: boolean,
  powerName: PowerName,
): ((a: Area) => Either<Area, Error>) =>
  flow(
    right,
    filterOrLeft(
      (a: Area) => !(flag && a.canal),
      constant(new Error("canal already true")),
    ),
    filterOrLeft(
      (a: Area) => flag || a.canal,
      constant(new Error("canal already false")),
    ),
    filterOrLeft(
      (a: Area) =>
        pipe(
          a.markers,
          get(powerName),
          exists((s: StatusMarker) => statusData[s.type].level >= 2),
        ),
      constant(new Error("影響下以上が必要")),
    ),
    map(evolve({ canal: constant(flag) })),
  );

export const isCasusBelli = (self: Area): boolean => {
  const possession =
    has(possessionMarker)(self.markers) ||
    has(stateMarker)(self.markers) ||
    has(dominionMarker)(self.markers);
  const changedPossession =
    has(possessionMarker)(self.changedMarkers) ||
    has(stateMarker)(self.changedMarkers) ||
    has(dominionMarker)(self.changedMarkers);
  return (
    (has(influenceMarker)(self.markers) &&
      (has(protectorateMarker)(self.markers) || possession)) ||
    (has(influenceMarker)(self.markers) &&
      (has(protectorateMarker)(self.changedMarkers) || changedPossession)) ||
    (has(protectorateMarker)(self.markers) &&
      (has(influenceMarker)(self.changedMarkers) ||
        has(protectorateMarker)(self.changedMarkers) ||
        changedPossession)) ||
    (possession &&
      (has(influenceMarker)(self.changedMarkers) ||
        has(protectorateMarker)(self.changedMarkers) ||
        changedPossession))
  );
};

export const isWarnArea = (self: Area): boolean =>
  !(
    (isEmpty(
      filter(self.markers, (s: StatusMarker) => statusData[s.type].level >= 4),
    ) ||
      isEmpty(
        filter(self.markers, (s: StatusMarker) => s.type === "Interest"),
      )) &&
    (isEmpty(
      filter(self.markers, (s: StatusMarker) => statusData[s.type].level >= 3),
    ) ||
      isEmpty(
        filter(self.markers, (s: StatusMarker) => s.type === "Influence"),
      ))
  );
