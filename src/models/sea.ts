import {
  type Either,
  filterOrLeft,
  fromOption,
  map,
  right,
} from "effect/Either";
import { constant, flow, pipe } from "effect/Function";
import type { HashMap } from "effect/HashMap";
import * as HM from "effect/HashMap";
import type { HashSet } from "effect/HashSet";
import * as HS from "effect/HashSet";
import { greaterThanOrEqualTo } from "effect/Number";
import * as O from "effect/Option";
import { some } from "effect/Option";
import { evolve } from "effect/Struct";

import type { PowerName } from "./types";
import type { AreaUnit } from "./unitType";

export interface SeaZone {
  merchants: HashSet<PowerName>;
  units: HashMap<AreaUnit, number>;
}

export const addMerchant = (
  power: PowerName,
): ((a: SeaZone) => Either<SeaZone, Error>) =>
  flow(
    right,
    filterOrLeft(
      (a: SeaZone) => !HS.has(power)(a.merchants),
      constant(new Error("already")),
    ),
    map(evolve({ merchants: HS.add(power) })),
  );

export const removeMerchant = (
  power: PowerName,
): ((a: SeaZone) => Either<SeaZone, Error>) =>
  flow(
    right,
    filterOrLeft(
      (a: SeaZone) => HS.has(power)(a.merchants),
      constant(new Error("not exists")),
    ),
    map(evolve({ merchants: HS.remove(power) })),
  );

export const addSeaUnit = (areaUnit: AreaUnit): ((a: SeaZone) => SeaZone) =>
  evolve({
    units: HM.modifyAt<AreaUnit, number>(
      areaUnit,
      O.match({
        onSome: (n) => some(n + 1),
        onNone: constant(some(1)),
      }),
    ),
  });

export const removeSeaUnit =
  (areaUnit: AreaUnit): ((a: SeaZone) => Either<SeaZone, Error>) =>
  (self: SeaZone): Either<SeaZone, Error> =>
    pipe(
      self,
      (x: SeaZone) => x.units,
      HM.get(areaUnit),
      O.filter(greaterThanOrEqualTo(1)),
      O.map(
        (n: number): SeaZone =>
          pipe(
            self,
            evolve({
              units: HM.set(areaUnit, n - 1),
            }),
          ),
      ),
      fromOption(constant(new Error("removeAreaUnit: unit count is minus"))),
    );
