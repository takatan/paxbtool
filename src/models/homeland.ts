import { Option } from "effect";
import { type Either, filterOrLeft, map, right } from "effect/Either";
import { constant, flow, pipe } from "effect/Function";
import { get, type HashMap, modifyAt } from "effect/HashMap";
import { getOrElse, orElse, some } from "effect/Option";
import { evolve } from "effect/Struct";

import {
  addTreasuryEvent,
  type TreasuryEvent,
  type TreasuryEvents,
  treasuryTotal,
} from "./treasury";
import type { UnitCounter } from "./unitType";
import { addVPEvent, type VPEvents, vpTotal } from "./vp";

export interface Homeland {
  merchant: number;
  units: HashMap<UnitCounter, number>;
  treasuryEvents: TreasuryEvents;
  vpEvents: VPEvents;
}

export const getUnit = (
  unitCounter: UnitCounter,
): ((self: Homeland) => number) =>
  flow((x) => x.units, get(unitCounter), getOrElse(constant(0)));

export const modifyUnit = (
  unitCounter: UnitCounter,
  diff: number,
): ((a: Homeland) => Either<Homeland, Error>) =>
  flow(
    right,
    filterOrLeft(
      () => unitCounter.type === "Army",
      constant(new Error("Homeland allows Only Army.")),
    ),
    map(
      evolve({
        units: modifyAt<UnitCounter, number>(
          unitCounter,
          flow(
            orElse(constant(some(0))),
            Option.map((n) => n + diff),
          ),
        ),
      }),
    ),
    filterOrLeft(
      (a: Homeland) => getUnit(unitCounter)(a) >= 0,
      constant(new Error("modifyUnit")),
    ),
  );

// TODO: 引数をTreasuryEventで受けるか
export const modifyTreasury =
  (amount: number, reason = "") =>
  (self: Homeland): Either<Homeland, Error> =>
    pipe(
      self,
      (x) => x.treasuryEvents,
      (events: TreasuryEvent[]) => addTreasuryEvent({ amount, reason })(events),
      map((events: TreasuryEvents) =>
        evolve(self, { treasuryEvents: constant(events) }),
      ),
    );

export const modifyVP = (
  vp: number,
  reason: string,
): ((a: Homeland) => Homeland) =>
  evolve({ vpEvents: addVPEvent({ vp, reason }) });

// TODO: 必要なAPIなのか?
export const modifyMerchant = (
  diff: number,
): ((a: Homeland) => Either<Homeland, Error>) =>
  flow(
    evolve({ merchant: (a) => a + diff }),
    right,
    filterOrLeft(
      (a: Homeland) => a.merchant >= 0,
      () => new Error("minus merchants"),
    ),
  );
export const getTreasuryTotal: (h: Homeland) => number = flow(
  (h) => h.treasuryEvents,
  treasuryTotal,
);
export const getVPTotal: (h: Homeland) => number = flow(
  (h) => h.vpEvents,
  vpTotal,
);
