export const playerPowerNames = [
  "Great Britain",
  "France",
  "Germany",
  "United States",
  "Japan",
  "Italy",
  "Russia",
] as const;
export const greatPowerNames = [
  ...playerPowerNames,
  "Austria-Hungary",
] as const;
export const homelandPowerNames = [
  ...greatPowerNames,
  "Spain",
  "Belgium",
  "Netherlands",
  "Portugal",
] as const;
export const powerNames = [
  ...homelandPowerNames,
  "Chinese Empire",
  "Ottoman Empire",
] as const;

export const seaNames = [
  "Baltic Sea",
  "North Atlantic Ocean",
  "Caribbean Sea",
  "South Atlantic Ocean",
  "Mediterranean",
  "Black Sea",
  "Indian Ocean",
  "North China Sea",
  "South China Sea",
  "Oceania",
  "North Pacific Ocean",
  "South Pacific Ocean",
  "Cape Horn",
  "Cape Hope",
] as const;

export const areaNames = [
  "Abyssinia",
  "Aden",
  "Afghanistan",
  "Alaska",
  "Algiers",
  "Anatolia",
  "Angola",
  "Argentina",
  "Ashantee",
  "Australia",
  "Baluchistan",
  "Bechuanaland",
  "Bengal",
  "Berbera",
  "Bismarck Arch.",
  "Bolivia",
  "Brasil",
  "Bulgaria",
  "Burma",
  "Canada",
  "Cape Colony",
  "Central America",
  "Central China",
  "Chile",
  "Cochin China",
  "Colombia",
  "Cuba",
  "Dutch East Indies",
  "Ecuador",
  "Egypt",
  "Eritrea",
  "Federated Malay States",
  "Fiji",
  "Formosa",
  "Fukien",
  "Gabon",
  "Gold Coast",
  "Greece",
  "Guiana",
  "Hawaii",
  "Hindoostan",
  "Hong Kong",
  "Indochina",
  "Inner Mongolia",
  "K.Wilhelm's Land",
  "Kambara",
  "Kamerun",
  "Kansu",
  "Kashmir",
  "Kenya",
  "Kongo",
  "Korea",
  "Kwang-Si",
  "Madagascar",
  "Manchuria",
  "Marocco",
  "Mauretania",
  "Mexico",
  "Mongolia",
  "Mozambique",
  "New Guinea",
  "New Hebrides",
  "New Zealand",
  "Newfoundland",
  "Nigeria",
  "Orange River FS ",
  "Panama",
  "Papua",
  "Persia",
  "Peru",
  "Philippines",
  "Port Arthur",
  "Porto Rico",
  "Punjab",
  "Quwait",
  "Rajputana",
  "Rhodesia",
  "Rio de Oro",
  "Rumania",
  "Sarawak",
  "Senegambia",
  "Serbia",
  "Shan-Tung",
  "Shang-Hai",
  "Siam",
  "Sin-Kiang",
  "Sokoto",
  "Somalia",
  "Soudan",
  "Sudwest Afrika",
  "Szech-Wan",
  "Tanganyika",
  "Taureg",
  "Tchad",
  "Tibet",
  "Togoland",
  "Transvaal",
  "Tripoli",
  "Tunis",
  "Turcomania",
  "Uganda",
  "United Provinces",
  "Venezuela ",
  "Yunnan",
] as const;

export type PlayerPowerName = (typeof playerPowerNames)[number];
export type GreatPowerName = (typeof greatPowerNames)[number];
export type HomelandPowerName = (typeof homelandPowerNames)[number];
export type PowerName = (typeof powerNames)[number];
export type SeaName = (typeof seaNames)[number];
export type AreaName = (typeof areaNames)[number];

// export const eqPowerName: Equivalence<PowerName> = stringEq;
// export const ordPowerName: Order<PowerName> = string;

export const vpDivisor: Record<PlayerPowerName, number> = {
  "Great Britain": 10,
  France: 7,
  Germany: 5,
  "United States": 4,
  Japan: 3,
  Italy: 2.5,
  Russia: 2.5,
};
