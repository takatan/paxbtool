import "./css/index.css";

import { createRoot } from "react-dom/client";

import { App } from "./App";

const container = document.querySelector("#root");
if (container === null) {
  throw new Error("Can not get container");
}
const root = createRoot(container); // createRoot(container!) if you use TypeScript
 
root.render(<App />);
