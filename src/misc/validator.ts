import { Schema } from "@effect/schema";
import * as EitherInstances from "@effect/typeclass/data/Either";
import * as Product from "@effect/typeclass/Product";
import { Array, Either, HashSet, Record } from "effect";
import { filterOrLeft, map, right } from "effect/Either";
import { constant, flow, pipe } from "effect/Function";
import { has, isEmpty, toEntries } from "effect/HashMap";
import { format } from "effect/Inspectable";

import { type Area, isCasusBelli } from "../models/area";
import type { SeaZone } from "../models/sea";
import {
  dominionMarker,
  stateMarker,
  statusData,
  type StatusMarker,
} from "../models/status";
import {
  type AreaName,
  type PowerName,
  powerNames,
  type SeaName,
} from "../models/types";
import type { AreaUnit } from "../models/unitType";
import type { WorldState } from "../models/worldState";
import { seaDataRecord } from "./database";

export const powerNameSchema = Schema.Union(Schema.Literal(...powerNames));

// const zs = Schema.struct<IJsonMainState>({
//   message:,
//   purchase:,
//   checkpoints:,
//   worldState:,
// });

export const stateArea: AreaName[] = ["Alaska", "Cuba", "Hawaii", "Porto Rico"];
export const dominionArea: AreaName[] = [
  "Australia",
  "Canada",
  "Cape Colony",
  "New Zealand",
];

const genericValidator =
  <K extends string, T>(
    vs: Array<([k]: [K, T]) => Either.Either<unknown, Error>>,
  ) =>
  (self: T, name: K) =>
    pipe(
      vs,
      Array.map((f: ([k, t]: [K, T]) => Either.Either<unknown, Error>) =>
        f([name, self]),
      ),
      Either.all,
      map(constant(self)),
    );

const areaValidator = genericValidator<AreaName, Area>([
  flow(
    right,
    filterOrLeft(
      ([n, a]: [AreaName, Area]) =>
        !(has(stateMarker)(a.markers) && !Array.contains(n)(stateArea)),
      ([n]) => new Error(`${n} 州マーカーが置けない`),
    ),
  ),
  flow(
    right,
    filterOrLeft(
      ([n, a]: [AreaName, Area]) =>
        !(has(dominionMarker)(a.markers) && !Array.contains(n)(dominionArea)),
      ([n]) => new Error(`${n} 自治領マーカーが置けない`),
    ),
  ),
]);

export const areaValidatorOnTurnEnd = genericValidator<AreaName, Area>([
  flow(
    right,
    filterOrLeft(
      ([, a]: [AreaName, Area]) => isEmpty(a.changedMarkers),
      ([n]) => new Error(`${n}: 未確定マーカー`),
    ),
  ),
  flow(
    right,
    filterOrLeft(
      ([, a]: [AreaName, Area]) => !a.unrest,
      ([n]) => new Error(`${n}: 暴動`),
    ),
  ),
  flow(
    right,
    filterOrLeft(
      ([, a]: [AreaName, Area]) => !isCasusBelli(a),
      ([n]) => new Error(`${n}: 宣戦理由`),
    ),
  ),
  // comprehension は Unitが空だとemptyになる場合とか困る
  ([n, a]: [AreaName, Area]) =>
    pipe(
      a.markers,
      toEntries,
      Array.map(
        flow(
          right,
          filterOrLeft(
            ([p, s]: [PowerName, StatusMarker]) =>
              !(
                statusData[s.type].level >= 3 &&
                pipe(
                  a.units,
                  toEntries,
                  Array.filter(
                    ([u, c]: [AreaUnit, number]) =>
                      u.power === p && u.type === "Army" && c > 0,
                  ),
                  Array.isEmptyArray,
                )
              ),
            ([p, s]: [PowerName, StatusMarker]) =>
              new Error(`${n}: ${format(s)}の${p}の守備隊`),
          ),
        ),
      ),
      Either.all,
    ),
]);

const seaValidatorOnTurnEnd = genericValidator([
  ([n, s]: [SeaName, SeaZone]) =>
    pipe(
      s.units,
      toEntries,
      Array.map(
        flow(
          right,
          filterOrLeft(
            ([u, c]: [AreaUnit, number]) =>
              !(
                u.type === "Fleet" &&
                c > 0 &&
                !HashSet.has(u.power)(seaDataRecord[n].homePowers)
              ),
            ([u]: [AreaUnit, number]) =>
              new Error(`${n}: 本国以外に海軍 ${format(u)}`),
          ),
        ),
      ),
      Either.all,
    ),
  ([n, s]: [SeaName, SeaZone]) =>
    pipe(
      s.units,
      toEntries,
      Array.map(
        flow(
          right,
          filterOrLeft(
            ([u, c]: [AreaUnit, number]) => !(u.type === "Army" && c > 0),
            ([u]: [AreaUnit, number]) =>
              new Error(`${n}: 海域に陸軍  ${format(u)}`),
          ),
        ),
      ),
      Either.all,
    ),
]);

const struct = Product.struct(EitherInstances.Product);

type Validator<K extends string, T> = (t: T, k: K) => Either.Either<T, Error>;

// traverseの引数が一引数の関数になっているためコピーする
const recordValidator = <K extends string, T>(
  record: Record<K, T>,
  validator: Validator<K, T>,
): Either.Either<Record<K, T>, Error> => {
  // eslint-disable-next-line @typescript-eslint/prefer-destructuring
  const F = EitherInstances.Applicative;
  return F.map(
    F.productAll(
      Record.toEntries(record).map(([key, a]) =>
        F.map(validator(a, key), (b) => [key, b] as const),
      ),
    ),
    Object.fromEntries,
  );
};

export const worldStateValidator = (
  self: WorldState,
): Either.Either<WorldState, Error> =>
  struct({
    areas: recordValidator(self.areas, areaValidator),
    homelands: right(self.homelands),
    seaZones: right(self.seaZones),
    stocks: right(self.stocks),
  });
export const worldStateValidatorOnTurnEnd = (
  self: WorldState,
): Either.Either<WorldState, Error> =>
  struct({
    areas: recordValidator(self.areas, areaValidatorOnTurnEnd),
    homelands: right(self.homelands),
    seaZones: recordValidator(self.seaZones, seaValidatorOnTurnEnd),
    stocks: right(self.stocks),
  });
