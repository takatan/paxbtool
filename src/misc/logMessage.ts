const logLevels = ["info", "error"] as const;
type LogLevel = (typeof logLevels)[number];
interface ILogMessage {
  level: LogLevel;
  body: string;
}
export type LogMessage = Readonly<ILogMessage>;
