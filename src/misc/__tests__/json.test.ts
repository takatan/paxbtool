import { Schema } from "@effect/schema";
import { format } from "effect/Inspectable";
import { none } from "effect/Option";
import { describe, expect, it } from "vitest";

import { startWorldState } from "../../store/startpos.ts";
import { mainStateToJson } from "../json.ts";
import { defaultAreaPosition, defaultSeaPosition } from "../position.ts";

describe("json", () => {
  const mainState = {
    checkpoints: [],
    position: {
      areaPosition: defaultAreaPosition,
      seaPosition: defaultSeaPosition,
    },
    snapshots: [],
    worldState: startWorldState,
    purchase: true,
    message: none(),
  };
  it.skip("a", () => {
    const json = format(mainStateToJson(mainState));
    expect(
      Schema.decodeUnknownSync(Schema.parseJson())(`{"a":"1"}`),
    ).toStrictEqual({
      a: "1",
    });
    // expect(mainState).toBe("");
    expect(json).toEqual("");
  });
  it("parse", () => {
    const jsonData = mainStateToJson(mainState);
    const json = format(jsonData);
    expect({
      // @ts-expect-error i know that
      ...Schema.decodeUnknownSync(Schema.parseJson())(json),
      message: undefined,
    }).toStrictEqual(jsonData);
  });
});
