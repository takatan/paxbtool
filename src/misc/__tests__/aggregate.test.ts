import { describe, expect, test } from "vitest";

import {
  sumOfMarker,
  sumOfUnitCost,
  sumOfValue,
  sumOfValueTotal,
} from "../../models/worldState";
import { startWorldState } from "../../store/startpos";

describe("aggregate", () => {
  test("集計", () => {
    const w = startWorldState;
    const gb = "Great Britain";
    expect(sumOfMarker("Possession")(gb)(w)).toBe(15);
    expect(sumOfValue("Possession")(gb)(w)).toBe(76);
    expect(sumOfUnitCost(gb)(w)).toBe(58);
    expect(sumOfValueTotal("Possession")(gb)(w)).toBe(380);
  });
});
