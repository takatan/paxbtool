import { Schema } from "@effect/schema";
import { struct } from "effect/Data";
import { flatMap, isLeft, isRight, map } from "effect/Either";
import { describe, expect, it, test } from "vitest";

import { addAreaUnit, addMarker, type Area } from "../../models/area";
import { protectorateMarker } from "../../models/status";
import { powerNames } from "../../models/types.ts";
import { defaultArea } from "../../store/default";
import { areaValidatorOnTurnEnd } from "../validator";

describe("Validator", () => {
  test("Area Validator", () => {
    const a = addMarker("Great Britain", protectorateMarker)(defaultArea);
    const b = map(
      addAreaUnit(
        struct({
          power: "Great Britain",
          type: "Army",
          strength: 1,
        }),
      ),
    )(a);
    expect(
      isLeft(
        flatMap((self: Area) => areaValidatorOnTurnEnd(self, "Guiana"))(a),
      ),
    ).toBeTruthy();
    expect(
      isRight(
        flatMap((self: Area) => areaValidatorOnTurnEnd(self, "Guiana"))(b),
      ),
    ).toBeTruthy();
  });
});

describe("schema", () => {
  it("test", () => {
    const s = Schema.Union(Schema.Literal(...powerNames));
    const x = Schema.decodeUnknownEither(s)("ab");
    expect(isLeft(x)).toBeTruthy();

    const y = Schema.decodeUnknownEither(s)("Germany");
    expect(isRight(y)).toBeTruthy();
    // const vs = Schema.string;

    // const zs: Schema.Schema<never, { a: string; b: number }> = Schema.struct({
    //   a: Schema.string,
    //   b: Schema.number,
    // });
    // const rzs = Schema.decodeUnknownEither(zs)("Germany");
    // console.log(rzs);
    // const rzs2 = Schema.decodeUnknownEither(zs)({ a: "a", b: 3 });
    // console.log(rzs2);

    // console.log(zs);
    // const z: Schema.Schema.Context<{ a: string }> = Schema.struct({
    //   a: Schema.string,
    // });
    // console.log(z);
  });
});
