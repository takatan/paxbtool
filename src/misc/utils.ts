const numberToPound = (index: number): string =>
  index.toLocaleString("ja-JP", {
    maximumFractionDigits: 0,
    minimumFractionDigits: 0,
    style: "currency",
    currency: "GBP",
  });
export default numberToPound;
