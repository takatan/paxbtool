import { Array, Either } from "effect";
import { map } from "effect/Either";
import { pipe } from "effect/Function";

import { type PowerName, powerNames } from "../models/types";
import type { WorldState } from "../models/worldState";

export interface DataRow<T> {
  label: string;
  data: readonly T[];
}

export const makeDataRow =
  <T>(
    label: string,
    f: (p: PowerName) => (w: WorldState) => Either.Either<T, Error>,
  ) =>
  (w: WorldState): Either.Either<DataRow<T>, Error> =>
    pipe(
      powerNames,
      Array.map((p: PowerName) => f(p)(w)),
      Either.all,
      map((d: readonly T[]) => ({
        label,
        data: d,
      })),
    );
