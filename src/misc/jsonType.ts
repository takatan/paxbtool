import type { HashMap } from "effect/HashMap";
import type { HashSet } from "effect/HashSet";

import type { Area } from "../models/area";
import type { Homeland } from "../models/homeland";
import type { SeaZone } from "../models/sea";
import type { Stock } from "../models/stock";
import type { AreaName, PowerName, SeaName } from "../models/types";
import type { LogMessage } from "./logMessage";

type JsonType<T> = {
  [P in keyof T]: T[P] extends HashMap<infer K, infer A>
    ? Array<[K, A]>
    : T[P] extends HashSet<infer S>
      ? S[]
      : T[P];
};

// TODO Iso使ってきれいにする？
export type IJsonSeaZone = JsonType<SeaZone>;
export type IJsonHomeland = JsonType<Homeland>;
export type IJsonArea = JsonType<Area>;
export type IJsonStock = JsonType<Stock>;

export interface IJsonWorldState {
  areas: Record<AreaName, IJsonArea>;
  seaZones: Record<SeaName, IJsonSeaZone>;
  homelands: Record<PowerName, IJsonHomeland>;
  stocks: Record<PowerName, IJsonStock>;
}
export interface IJsonMainState {
  worldState: IJsonWorldState;
  message?: LogMessage;
  purchase: boolean;
  checkpoints: IJsonWorldState[];
}
