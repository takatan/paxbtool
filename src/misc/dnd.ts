import { type Either, flatMap } from "effect/Either";
import { flow } from "effect/Function";

import type { StatusMarker } from "../models/status";
import type { PowerName } from "../models/types";
import type { AreaUnit } from "../models/unitType";
import type { WorldState } from "../models/worldState";
import { doModifyWorldFinish, type MainState } from "../store/main";

// StockFooはStockからTrash行きを防ぐため
const dnd = [
  "Unit",
  "StockUnit",
  "Merchant",
  "StockMerchant",
  "Status",
  "StockStatus",
  "Canal",
  "AreaBox",
] as const;
type DndType = (typeof dnd)[number];

interface IDragTokenType {
  type: DndType;
}

export interface IDndUnit extends IDragTokenType {
  type: "Unit";
  send: (w: WorldState) => Either<WorldState, Error>;
  areaUnit: AreaUnit;
}
export interface IDndStockUnit extends IDragTokenType {
  type: "StockUnit";
  send: (w: WorldState) => Either<WorldState, Error>;
  areaUnit: AreaUnit;
}
export interface IDndMerchant extends IDragTokenType {
  type: "Merchant";
  send: (w: WorldState) => Either<WorldState, Error>;
  powerName: PowerName;
}
export interface IDndStockMerchant extends IDragTokenType {
  type: "StockMerchant";
  send: (w: WorldState) => Either<WorldState, Error>;
  powerName: PowerName;
}
export interface IDndStatus extends IDragTokenType {
  type: "Status";
  send: (w: WorldState) => Either<WorldState, Error>;
  powerName: PowerName;
  statusMarker: StatusMarker;
}
export interface IDndStockStatus extends IDragTokenType {
  type: "StockStatus";
  send: (w: WorldState) => Either<WorldState, Error>;
  powerName: PowerName;
  statusMarker: StatusMarker;
}
export interface IDndCanal extends IDragTokenType {
  type: "Canal";
  send: (w: WorldState) => Either<WorldState, Error>;
  powerName: PowerName;
}
export interface IDndAreaBox extends IDragTokenType {
  type: "AreaBox";
}
export const doSendRecv = (
  send: (w: WorldState) => Either<WorldState, Error>,
  recv: (w: WorldState) => Either<WorldState, Error>,
  message: string,
): ((m: MainState) => MainState) =>
  doModifyWorldFinish({
    message,
    modifier: flow(send, flatMap(recv)),
  });
