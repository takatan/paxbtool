import { Array, HashSet } from "effect";
import { map as mapA } from "effect/Array";
import { toEntries } from "effect/HashMap";
import { getOrUndefined } from "effect/Option";
import { map } from "effect/Record";

import type { Area } from "../models/area";
import type { Homeland } from "../models/homeland";
import type { SeaZone } from "../models/sea";
import type { Stock } from "../models/stock";
import type { WorldState } from "../models/worldState";
import type { MainState } from "../store/main";
import type {
  IJsonArea,
  IJsonHomeland,
  IJsonMainState,
  IJsonSeaZone,
  IJsonStock,
  IJsonWorldState,
} from "./jsonType";

//
const seaZoneToJson = (sea: SeaZone): IJsonSeaZone => ({
  merchants: Array.fromIterable(HashSet.values(sea.merchants)),
  units: toEntries(sea.units),
});
const homelandToJson = (homeland: Homeland): IJsonHomeland => ({
  ...homeland,
  units: toEntries(homeland.units),
});
const areaToJson = (area: Area): IJsonArea => ({
  ...area,
  markers: toEntries(area.markers),
  changedMarkers: toEntries(area.changedMarkers),
  units: toEntries(area.units),
});

const stockToJson = (stock: Stock): IJsonStock => ({
  ...stock,
});

const worldStateToJson = (worldState: WorldState): IJsonWorldState => ({
  areas: map(areaToJson)(worldState.areas),
  seaZones: map(seaZoneToJson)(worldState.seaZones),
  homelands: map(homelandToJson)(worldState.homelands),
  stocks: map(stockToJson)(worldState.stocks),
});
export const mainStateToJson = (mainState: MainState): IJsonMainState => ({
  ...mainState,
  worldState: worldStateToJson(mainState.worldState),
  checkpoints: mapA(worldStateToJson)(mainState.checkpoints),
  message: getOrUndefined(mainState.message),
});
