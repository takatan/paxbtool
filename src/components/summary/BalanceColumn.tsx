import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  List,
  ListItem,
  MenuItem,
  Popover,
  Select,
  TextField,
} from "@mui/material";
import ListItemText from "@mui/material/ListItemText";
import type { SelectChangeEvent } from "@mui/material/Select/SelectInput";
import { map, range } from "effect/Array";
import { pipe } from "effect/Function";
import * as React from "react";
import { useCallback } from "react";
import { useSetRecoilState } from "recoil";

import numberToPound from "../../misc/utils";
import { modifyTreasury } from "../../models/homeland";
import {
  type TreasuryEvent,
  type TreasuryEvents,
  treasuryTotal,
} from "../../models/treasury";
import type { GreatPowerName, PowerName } from "../../models/types";
import { modifyHomeland } from "../../models/worldState";
import { doModifyWorldFinish, mainSate } from "../../store/main";

interface IBalanceColumnProperties {
  powerName: GreatPowerName;
  treasuryEvents: TreasuryEvents;
}

const officeIncomeTable = (powerName: GreatPowerName): number[] => {
  switch (powerName) {
    case "Great Britain":
    case "France": {
      return [15, 20, 25, 35, 40, 45];
    }
    case "Germany":
    case "United States": {
      return [10, 15, 20, 20, 25, 30];
    }
    case "Austria-Hungary":
    case "Japan":
    case "Italy":
    case "Russia": {
      return [5, 7, 10, 10, 20, 30];
    }
    default: {
      throw new Error(powerName satisfies never);
    }
  }
};

export const BalanceColumn: React.FC<IBalanceColumnProperties> = ({
  powerName,
  treasuryEvents,
}) => {
  const setMain = useSetRecoilState(mainSate);
  const addTreasuryEvent = useCallback(
    (powerName: PowerName, amount: number, reason: string): void => {
      setMain(
        doModifyWorldFinish({
          modifier: modifyHomeland(modifyTreasury(amount, reason))(powerName),
          message: "addVPEvent",
        }),
      );
    },
    [setMain],
  );

  const [amount, setAmount] = React.useState<string>("");
  const [reason, setReason] = React.useState<string>("");
  const [select, setSelect] = React.useState<string>("その他");
  const [anchorElement, setAnchorElement] =
    React.useState<HTMLButtonElement | null>(null);
  const onSelectChange = React.useCallback(
    (event: SelectChangeEvent) => {
      const { value } = event.target;
      setSelect(value);
      const index = Number.parseInt(value, 10);
      if (index >= 1 && index <= 6) {
        setAmount(officeIncomeTable(powerName)[index - 1].toString());
        setReason(`植民省:D${index}`);
      } else {
        setAmount("0");
        setReason("");
      }
    },
    [powerName],
  );
  const onAmountChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setAmount(event.target.value);
    },
    [],
  );
  const onReasonChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setReason(event.target.value);
    },
    [],
  );

  const onClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorElement(event.currentTarget);
    },
    [],
  );
  const onClose = React.useCallback(() => {
    setAnchorElement(null);
  }, []);
  const onSubmit = React.useCallback(() => {
    onClose();
    addTreasuryEvent(powerName, Number.parseInt(amount, 10), reason);
    setReason("");
    setAmount("0");
    setSelect("その他");
  }, [powerName, amount, reason, addTreasuryEvent, onClose]);
  return (
    <Grid item={true} xs={true}>
      <List>
        <ListItem>
          <ListItemText>
            国庫
            {pipe(treasuryEvents, treasuryTotal, numberToPound)}
          </ListItemText>
          <ListItemText>
            <Button
              variant="outlined"
              onClick={onClick}
              className="modify-button"
            >
              国庫修正
            </Button>
          </ListItemText>
        </ListItem>
        {map(treasuryEvents, (f: TreasuryEvent, index: number) => (
          <ListItem key={index}>
            <ListItemText>{numberToPound(f.amount)}</ListItemText>
            <ListItemText primary={f.reason} />
          </ListItem>
        ))}
      </List>
      <Popover
        open={Boolean(anchorElement)}
        anchorEl={anchorElement}
        onClose={onClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <FormControl>
          <InputLabel>テンプレート</InputLabel>
          <Select value={select} onChange={onSelectChange}>
            <MenuItem value="その他">その他</MenuItem>
            {map((index: number) => {
              const v = officeIncomeTable(powerName);
              return (
                <MenuItem value={index} key={index}>
                  D{index}/{v[index - 1]}
                </MenuItem>
              );
            })(range(1, 6))}
          </Select>
        </FormControl>
        <br />
        <TextField
          required={true}
          label="金額"
          value={amount}
          onChange={onAmountChange}
          type="number"
        />
        <br />
        <TextField
          id="standard-name"
          label="理由"
          value={reason}
          onChange={onReasonChange}
          margin="normal"
        />
        <br />
        <Button variant="contained" color="primary" onClick={onSubmit}>
          登録
        </Button>
      </Popover>
    </Grid>
  );
};
