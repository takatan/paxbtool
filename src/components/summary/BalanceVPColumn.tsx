import {
  Button,
  Grid,
  List,
  ListItem,
  Popover,
  TextField,
} from "@mui/material";
import ListItemText from "@mui/material/ListItemText";
import { map } from "effect/Array";
import { right } from "effect/Either";
import { flow } from "effect/Function";
import * as React from "react";
import { useSetRecoilState } from "recoil";

import { powerToClassName } from "../../misc/database";
import numberToPound from "../../misc/utils";
import { modifyVP } from "../../models/homeland";
import { type PlayerPowerName, vpDivisor } from "../../models/types";
import { type VPEvent, type VPEvents, vpTotal } from "../../models/vp";
import { modifyHomeland } from "../../models/worldState";
import { doModifyWorldFinish, mainSate } from "../../store/main.ts";

interface IBalanceVPColumnProperties {
  powerName: PlayerPowerName;
  sum: number;
  vpEvents: VPEvents;
}
const BalanceVPColumn: React.FC<IBalanceVPColumnProperties> = (properties) => {
  const setMain = useSetRecoilState(mainSate);
  const { powerName, sum, vpEvents } = properties;
  const [amount, setAmount] = React.useState<string>("");
  const [reason, setReason] = React.useState<string>("");
  const [anchorElement, setAnchorElement] =
    React.useState<HTMLButtonElement | null>(null);
  const onAmountChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setAmount(event.target.value);
    },
    [],
  );
  const onReasonChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setReason(event.target.value);
    },
    [],
  );

  const onClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorElement(event.currentTarget);
    },
    [],
  );
  const onClose = React.useCallback(() => {
    setAnchorElement(null);
  }, []);
  const onSubmit = React.useCallback(() => {
    onClose();
    setMain(
      doModifyWorldFinish({
        modifier: modifyHomeland(
          flow(modifyVP(Number.parseInt(amount, 10), reason), right),
        )(powerName),
        message: "addVPEvent",
      }),
    );
    setReason("");
    setAmount("0");
  }, [amount, onClose, powerName, reason, setMain]);

  const vp = Math.floor(sum / vpDivisor[powerName]);
  const vpSum = vp + vpTotal(vpEvents);
  return (
    <Grid xs={true} item={true}>
      <div>
        <span
          className={[powerToClassName(powerName), "country-box"].join(" ")}
        />
        {powerName}
      </div>
      <div>
        <List>
          <ListItem>
            <ListItemText>VP={vpSum}</ListItemText>
            <ListItemText>
              <Button
                variant="outlined"
                onClick={onClick}
                className="modify-button"
              >
                VP修正
              </Button>
            </ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>{vp}</ListItemText>
            <ListItemText>
              国庫仮VP: {numberToPound(sum)}/{vpDivisor[powerName]}
            </ListItemText>
          </ListItem>
        </List>
        <List>
          {map((e: VPEvent, index: number) => (
            <ListItem key={index}>
              <ListItemText>{e.vp}</ListItemText>
              <ListItemText primary={e.reason} />
            </ListItem>
          ))(vpEvents)}
        </List>
        <Popover
          open={Boolean(anchorElement)}
          anchorEl={anchorElement}
          onClose={onClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <br />
          <TextField
            required={true}
            label="VP修正"
            value={amount}
            onChange={onAmountChange}
            type="number"
          />
          <br />
          <TextField
            id="standard-name"
            label="理由"
            value={reason}
            onChange={onReasonChange}
            margin="normal"
          />
          <br />
          <Button variant="outlined" color="primary" onClick={onSubmit}>
            登録
          </Button>
        </Popover>
      </div>
    </Grid>
  );
};
export default BalanceVPColumn;
