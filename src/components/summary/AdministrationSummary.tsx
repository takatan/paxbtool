import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { Record } from "effect";
import { map } from "effect/Array";
import { flow } from "effect/Function";
import * as React from "react";
import { useRecoilValue } from "recoil";

import numberToPound from "../../misc/utils";
import { statusData, type StatusType, statusTypes } from "../../models/status";
import { type GreatPowerName, greatPowerNames } from "../../models/types";
import {
  sumOfCostTotal,
  sumOfMarker,
  sumOfUnitCost,
  sumOfValue,
  sumOfValueTotal,
  totalBalance,
  totalCost,
  totalIncome,
} from "../../models/worldState";
import { mainSate } from "../../store/main";

interface Income {
  n: number;
  valueSum: number;
  income: number;
  costTotal: number;
}

type IncomeRecord = Record<StatusType, Income>;
interface Balance {
  incomeRecord: IncomeRecord;
  unitCost: number;
  totalIncome: number;
  totalCost: number;
  balance: number;
}
type AdministrationRecord = Record<GreatPowerName, Balance>;

const AdministrationSummary = (): React.JSX.Element => {
  const world = useRecoilValue(mainSate).worldState;
  const administrationRecord: AdministrationRecord = React.useMemo(
    () =>
      Record.fromIterableWith(
        greatPowerNames,
        (p: GreatPowerName): [GreatPowerName, Balance] => [
          p,
          {
            incomeRecord: Record.fromIterableWith(
              statusTypes,
              (s: StatusType): [StatusType, Income] => [
                s,
                {
                  n: sumOfMarker(s)(p)(world),
                  valueSum: sumOfValue(s)(p)(world),
                  income: sumOfValueTotal(s)(p)(world),
                  costTotal: sumOfCostTotal(s)(p)(world),
                },
              ],
            ) as IncomeRecord, // FIXME
            unitCost: sumOfUnitCost(p)(world),
            totalIncome: totalIncome(p)(world),
            totalCost: totalCost(p)(world),
            balance: totalBalance(p)(world),
          },
        ],
      ) as AdministrationRecord, // FIXME

    [world],
  );
  const parity = (n: number): "odd" | "even" => (n % 2 === 0 ? "odd" : "even");
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>収入集計</TableCell>
          {map((p: GreatPowerName, index: number) => (
            <TableCell className={`admin-powername ${parity(index)}`}>
              {p}
            </TableCell>
          ))(greatPowerNames)}
        </TableRow>
      </TableHead>
      <TableBody>
        {map((s: StatusType) => (
          <TableRow>
            <TableCell variant="head">
              {statusData[s].text}(係数{statusData[s].income} コスト
              {statusData[s].cost})
            </TableCell>
            {flow(
              map((p: GreatPowerName) => administrationRecord[p]),
              map((d: Balance, index: number) =>
                // この条件はなんだったか忘れた。
                // eslint-disable-next-line no-constant-condition
                d.incomeRecord[s].n > 0 || true ? (
                  <TableCell className={`number ${parity(index)}`}>
                    {d.incomeRecord[s].n} /
                    {numberToPound(d.incomeRecord[s].valueSum)} /
                    {numberToPound(d.incomeRecord[s].income)} / -
                    {numberToPound(d.incomeRecord[s].costTotal)}
                  </TableCell>
                ) : (
                  <TableCell className={parity(index)} />
                ),
              ),
            )(greatPowerNames)}
          </TableRow>
        ))(statusTypes)}
        <TableRow>
          <TableCell variant="head">軍事費</TableCell>
          {map((p: GreatPowerName, index: number) => (
            <TableCell className={`number ${parity(index)}`}>
              -{numberToPound(administrationRecord[p].unitCost)}
            </TableCell>
          ))(greatPowerNames)}
        </TableRow>
        <TableRow>
          <TableCell variant="head">総収支</TableCell>
          {map((p: GreatPowerName, index: number) => (
            <TableCell className={`number ${parity(index)}`}>
              {numberToPound(administrationRecord[p].totalIncome)} -
              {numberToPound(administrationRecord[p].totalCost)} =
              {numberToPound(administrationRecord[p].balance)}
            </TableCell>
          ))(greatPowerNames)}
        </TableRow>
      </TableBody>
    </Table>
  );
};
export default AdministrationSummary;
