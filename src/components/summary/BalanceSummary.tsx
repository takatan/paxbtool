import { Grid } from "@mui/material";
import { Either } from "effect";
import { findFirst, map } from "effect/Array";
import { getOrThrowWith, map as mapE } from "effect/Either";
import { constant, pipe } from "effect/Function";
import { getOrElse, map as mapO } from "effect/Option";
import * as React from "react";
import { useRecoilValue } from "recoil";

import { powerToClassName } from "../../misc/database";
import { type TreasuryEvents, treasuryTotal } from "../../models/treasury";
import { type GreatPowerName, greatPowerNames } from "../../models/types";
import type { VPEvents } from "../../models/vp";
import { getHomeland } from "../../models/worldState";
import { mainSate } from "../../store/main.ts";
import { BalanceColumn } from "./BalanceColumn";
import BalanceVPColumn from "./BalanceVPColumn";

interface IData {
  powerName: GreatPowerName;
  events: TreasuryEvents;
  vpEvents: VPEvents;
}

const BalanceSummary: React.FC = () => {
  const world = useRecoilValue(mainSate).worldState;
  const dataList: Array<{
    powerName: GreatPowerName;
    events: TreasuryEvents;
    vpEvents: VPEvents;
  }> = React.useMemo(
    () =>
      pipe(
        greatPowerNames,
        map((powerName: GreatPowerName) =>
          mapE(
            Either.all([
              getHomeland((x) => x.treasuryEvents)(powerName)(world),
              getHomeland((x) => x.vpEvents)(powerName)(world),
            ]),
            ([events, vpEvents]: [TreasuryEvents, VPEvents]) => ({
              powerName,
              events,
              vpEvents,
            }),
          ),
        ),
        Either.all,
        getOrThrowWith((e: Error) => {
          throw e;
        }),
      ),
    [world],
  );

  const aus = React.useMemo(
    () =>
      pipe(
        dataList,
        findFirst((d: IData) => d.powerName === "Austria-Hungary"),
        mapO((d: IData) => treasuryTotal(d.events)),
        getOrElse(constant(0)),
      ),
    [dataList],
  );
  return (
    <div>
      <Grid container={true}>
        {map(dataList, ({ powerName, events, vpEvents }: IData) => {
          switch (powerName) {
            case "Austria-Hungary": {
              return (
                <Grid xs={true} item={true} key={powerName}>
                  <div>
                    <span
                      className={[
                        powerToClassName(powerName),
                        "country-box",
                      ].join(" ")}
                    />
                    {powerName}
                  </div>
                </Grid>
              );
            }
            case "Germany": {
              return (
                <BalanceVPColumn
                  key={powerName}
                  powerName={powerName}
                  sum={treasuryTotal(events) + aus}
                  vpEvents={vpEvents}
                />
              );
            }
            default: {
              return (
                <BalanceVPColumn
                  key={powerName}
                  powerName={powerName}
                  sum={treasuryTotal(events)}
                  vpEvents={vpEvents}
                />
              );
            }
          }
        })}
      </Grid>
      <Grid container={true}>
        {map(dataList, ({ powerName, events }: IData) => (
          <BalanceColumn
            key={powerName}
            powerName={powerName}
            treasuryEvents={events}
          />
        ))}
      </Grid>
    </div>
  );
};

export default BalanceSummary;
