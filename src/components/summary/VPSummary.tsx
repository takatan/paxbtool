import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { Either } from "effect";
import { map, range } from "effect/Array";
import { flow, pipe } from "effect/Function";
import { sumAll } from "effect/Number";
import * as React from "react";
import { useRecoilValue } from "recoil";

import { type PlayerPowerName, playerPowerNames } from "../../models/types";
import { homelandGetVP, type WorldState } from "../../models/worldState";
import { mainSate } from "../../store/main";

const VPSummary: React.FC = () => {
  const { checkpoints } = useRecoilValue(mainSate);
  const dataList = React.useMemo(
    () =>
      pipe(
        checkpoints,
        map((worldState: WorldState) =>
          flow(
            map((p: PlayerPowerName) => homelandGetVP(p)(worldState)),
            Either.all,
          )(playerPowerNames),
        ),
        Either.all,
        Either.getOrThrowWith((e) => {
          throw e;
        }),
      ),
    [checkpoints],
  );

  // TODO: なんかいまいちな集約方法
  const totals = React.useMemo(
    () =>
      pipe(
        playerPowerNames,
        map((_, index: number) =>
          sumAll(
            map((t: number) => dataList[t][index])(
              range(0, dataList.length - 1),
            ),
          ),
        ),
      ),
    [dataList],
  );
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>VP</TableCell>
          {map((p: PlayerPowerName) => <TableCell key={p}>{p}</TableCell>)(
            playerPowerNames,
          )}
        </TableRow>
      </TableHead>
      <TableBody>
        {map((vps: readonly number[], index: number) => (
          <TableRow key={index}>
            <TableCell>
              year={1880 + index * 4} turn{index + 1}
            </TableCell>
            {map((vp: number, index: number) => (
              <TableCell key={index}>{vp}</TableCell>
            ))(vps)}
          </TableRow>
        ))(dataList)}
        <TableRow>
          <TableCell>total</TableCell>
          {map((total: number, index: number) => (
            <TableCell key={index}>{total}</TableCell>
          ))(totals)}
        </TableRow>
      </TableBody>
    </Table>
  );
};
export default VPSummary;
