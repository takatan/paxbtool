import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { Either } from "effect";
import { flatten, map } from "effect/Array";
import { getOrThrowWith } from "effect/Either";
import * as React from "react";
import { useRecoilValue } from "recoil";

import { type DataRow, makeDataRow } from "../../misc/aggregate";
import { type StatusShape, statusShapes } from "../../models/status";
import { type PowerName, powerNames } from "../../models/types";
import { type UnitStrength, unitStrengths } from "../../models/unitType";
import {
  stockGetMerchant,
  stockGetStatusMarker,
  stockGetUnit,
} from "../../models/worldState";
import { mainSate } from "../../store/main";

const StockSummary: React.FC = () => {
  const world = useRecoilValue(mainSate).worldState;
  const rows = React.useMemo(
    (): Array<DataRow<number>> =>
      getOrThrowWith(
        Either.all(
          flatten([
            [makeDataRow("商船", stockGetMerchant)(world)],
            map(statusShapes, (s: StatusShape) =>
              makeDataRow(`マーカー ${s}`, stockGetStatusMarker(s))(world),
            ),
            map(unitStrengths, (s: UnitStrength) =>
              makeDataRow(`軍隊(${s})`, stockGetUnit(s))(world),
            ),
          ]),
        ),
        (e) => {
          throw e;
        },
      ),
    [world],
  );

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>手元ストック</TableCell>
          {map((p: PowerName) => <TableCell key={p}>{p}</TableCell>)(
            powerNames,
          )}
        </TableRow>
      </TableHead>
      <TableBody>
        {map((r: DataRow<number>) => (
          <TableRow key={r.label}>
            <TableCell>{r.label}</TableCell>
            {map((d: number, index: number) => (
              <TableCell key={index}>{d}</TableCell>
            ))(r.data)}
          </TableRow>
        ))(rows)}
      </TableBody>
    </Table>
  );
};

export default StockSummary;
