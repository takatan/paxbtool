import { Menu } from "@mui/icons-material";
import {
  AppBar,
  Button,
  Drawer,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";
import * as React from "react";

import ErrorNotification from "./ErrorNotification";
import PBAppBar from "./PBAppBar";
import SideDrawer from "./SideDrawer";
import AdministrationSummary from "./summary/AdministrationSummary";
import BalanceSummary from "./summary/BalanceSummary";
import StockSummary from "./summary/StockSummary";
import VPSummary from "./summary/VPSummary";
import WorldMap from "./WorldMap";

const PaxBritannicaApp: React.FC = () => {
  const [value, setValue] = React.useState(0);
  const [sideDrawer, setSideDrawer] = React.useState(false);

  const onSideDrawerClose = React.useCallback(() => {
    setSideDrawer(false);
  }, []);

  const setDrawerValue = React.useCallback(
    (n: number) => () => {
      setValue(n);
    },
    [],
  );
  const onKeyPress = React.useCallback((event: React.KeyboardEvent): void => {
    switch (event.key) {
      case "1": {
        setValue(1);
        break;
      }
      case "2": {
        setValue(2);
        break;
      }
      default: {
        break;
      }
    }
  }, []);
  const onMenuClick = React.useCallback(() => {
    setSideDrawer(true);
  }, []);

  return (
    <div onKeyDown={onKeyPress}>
      <div>
        <AppBar position="static">
          <Toolbar variant="dense">
            <IconButton
              edge="start"
              color="inherit"
              aria-label="Menu"
              onClick={onMenuClick}
            >
              <Menu />
            </IconButton>
            <Button onClick={setDrawerValue(1)} variant="contained">
              国庫・VP収支
            </Button>
            <Button onClick={setDrawerValue(2)} variant="contained">
              VP履歴
            </Button>
            <Button onClick={setDrawerValue(3)} variant="contained">
              行政記録
            </Button>
            <Button onClick={setDrawerValue(4)} variant="contained">
              ストック
            </Button>
            <Typography variant="body2" className="notice">
              開発中: 要望・バグは
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://gitlab.com/takatan/paxbtool/issues"
              >
                Gitlabの課題トラッカー
              </a>
              に登録して下さい。 Copyright(c) 2019- TANIGUCHI Takaki: This
              program is distributed under the GPL-3 or later.
            </Typography>
          </Toolbar>
        </AppBar>
        <SideDrawer open={sideDrawer} onClose={onSideDrawerClose} />
        <Drawer anchor="top" open={value > 0} onClose={setDrawerValue(0)}>
          <div className="top-drawer">
            {value === 1 && <BalanceSummary />}
            {value === 2 && <VPSummary />}
            {value === 3 && <AdministrationSummary />}
            {value === 4 && <StockSummary />}
          </div>
        </Drawer>

        <WorldMap />
      </div>
      <ErrorNotification />
      <PBAppBar />
    </div>
  );
};

export default PaxBritannicaApp;
