import { Grid } from "@mui/material";
import Tooltip from "@mui/material/Tooltip";
import { Array, Option, Record } from "effect";
import { flatten, map } from "effect/Array";
import { pipe } from "effect/Function";
import { none, some } from "effect/Option";
import { collect } from "effect/Record";
import { Order } from "effect/String";
import * as React from "react";
import { useMemo } from "react";
import { Layer, Line, Stage } from "react-konva";
import { useRecoilValue } from "recoil";

import {
  type AreaData,
  areaDataRecord,
  powerDataRecord,
  powerHomeSeaRecord,
  powerToClassName,
  type SeaData,
  seaDataRecord,
} from "../misc/database";
import { homelandPosition } from "../misc/position";
import useWindowDimensions from "../misc/reactUse";
import type { Area } from "../models/area";
import type { SeaZone } from "../models/sea";
import {
  type AreaName,
  type HomelandPowerName,
  homelandPowerNames,
  type SeaName,
} from "../models/types";
import { mainSate } from "../store/main";
import AreaBox from "./box/AreaBox";
import HomelandBox from "./box/HomelandBox";
import SeaZoneBox from "./box/SeaZoneBox";

const abbrToAreaName: Record<string, AreaName> = Record.mapEntries(
  areaDataRecord,
  (a: AreaData, k: AreaName) => [a.abbreviation, k],
);
const abbrToHomelandPowerName: Record<string, HomelandPowerName> = {
  USA: "United States",
  Russ: "Russia",
  "A.H.": "Austria-Hungary",
};

const WorldMap = (): React.JSX.Element => {
  const {
    worldState: world,
    position: { areaPosition, seaPosition },
  } = useRecoilValue(mainSate);
  const { areas, seaZones, homelands } = world;
  const { width, height } = useWindowDimensions();
  const winSize = useMemo(
    () => ({ width, height: height * 0.82 }),
    [height, width],
  );
  const areaPoints: Array<[number, number, number, number]> = React.useMemo(
    (): Array<[number, number, number, number]> =>
      pipe(
        areaDataRecord,
        collect((k: AreaName, a: AreaData) =>
          pipe(
            a.adjacencies,
            Array.filterMap(
              (adj: string): Option.Option<AreaName> =>
                Record.get(adj)(abbrToAreaName),
            ),
            Array.filterMap(
              (
                an: AreaName,
              ): Option.Option<[number, number, number, number]> =>
                Order(an, k) > 0
                  ? some([
                      winSize.width * (areaPosition[k].left * 0.01 + 0.02),
                      winSize.height * (areaPosition[k].top * 0.01 + 0.01),
                      winSize.width * (areaPosition[an].left * 0.01 + 0.02),
                      winSize.height * (areaPosition[an].top * 0.01 + 0.01),
                    ])
                  : none(),
            ),
          ),
        ),
        flatten,
      ),
    [areaPosition, winSize],
  );
  const homePoints: Array<[number, number, number, number]> = React.useMemo(
    () =>
      pipe(
        areaDataRecord,
        collect((k: AreaName, a: AreaData) =>
          pipe(
            a.adjacencies,
            Array.filterMap((adj: string) =>
              Record.get(adj)(abbrToHomelandPowerName),
            ),
            map((an: HomelandPowerName): [number, number, number, number] => [
              winSize.width * (areaPosition[k].left * 0.01 + 0.02),
              winSize.height * (areaPosition[k].top * 0.01 + 0.01),
              homelandPosition[an].left * winSize.width * 0.01 + 10,
              homelandPosition[an].top * winSize.height * 0.01 + 10,
            ]),
          ),
        ),
        flatten,
      ),
    [areaPosition, winSize],
  );
  const seaPoints: Array<[number, number, number, number]> = React.useMemo(
    () =>
      pipe(
        seaDataRecord,
        collect((k: SeaName, a: SeaData) =>
          pipe(
            a.via,
            Array.filterMap((an: SeaName) =>
              pipe(
                [
                  winSize.width * (seaPosition[k].left * 0.01 + 0.03),
                  winSize.height * (seaPosition[k].top * 0.01 + 0.02),
                  winSize.width * (seaPosition[an].left * 0.01 + 0.03),
                  winSize.height * (seaPosition[an].top * 0.01 + 0.02),
                ],
                some,
                Option.filter(
                  ([x0, y0, x1, y1]: number[]) =>
                    x0 < x1 || (x0 === x1 && y0 < y1),
                ),
                Option.map(
                  ([x0, y0, x1, y1]): Array<
                    [number, number, number, number]
                  > =>
                    Math.abs(x1 - winSize.width - x0) < Math.abs(x1 - x0)
                      ? [
                          [x0, y0, x1 - winSize.width, y1],
                          [x1, y1, x0 + winSize.width, y0],
                        ]
                      : [[x0, y0, x1, y1]],
                ),
              ),
            ),
            flatten,
          ),
        ),
        flatten,
      ),
    [winSize, seaPosition],
  );
  return (
    <div id="worldmap">
      <div id="areamap">
        <Stage width={winSize.width} height={winSize.height}>
          <Layer>
            {map((p: number[], index: number) => (
              <Line
                key={index}
                points={p}
                stroke="#bbbbee"
                strokeWidth={1}
                dash={[3, 3]}
              />
            ))(seaPoints)}
            {map((p: number[], index: number) => (
              <Line
                key={index}
                points={p}
                stroke="black"
                strokeWidth={2}
                dash={[4, 8]}
              />
            ))(areaPoints)}
            {map((p: number[], index: number) => (
              <Line
                key={index}
                points={p}
                stroke="green"
                strokeWidth={1}
                dash={[3, 3]}
              />
            ))(homePoints)}
          </Layer>
        </Stage>
        {map((p: HomelandPowerName, index: number) => (
          <Tooltip title={powerHomeSeaRecord[p].join(", ")} key={index}>
            <span
              style={{
                position: "absolute",
                top: homelandPosition[p].top * winSize.height * 0.01,
                left: homelandPosition[p].left * winSize.width * 0.01,
              }}
              className={`${powerToClassName(p)} homeland-marker`}
              id={`homeland-${powerDataRecord[p].abbr}`}
            >
              <span className="label">{powerDataRecord[p].abbr}</span>
            </span>
          </Tooltip>
        ))(homelandPowerNames)}
        {collect((areaName: AreaName, area: Area) => (
          <AreaBox areaName={areaName} area={area} key={areaName} />
        ))(areas)}
        {collect((index: SeaName, s: SeaZone) => (
          <SeaZoneBox key={index} seaName={index} seaZone={s} />
        ))(seaZones)}
      </div>
      <Grid className="homeland" container={true}>
        {
          // TODO Record[i] を使うのは本当は良くないか
          map((index: HomelandPowerName) => (
            <HomelandBox
              key={index}
              homeland={homelands[index]}
              powerName={index}
            />
          ))(homelandPowerNames)
        }
      </Grid>
    </div>
  );
};
export default WorldMap;
