import { Menu, MenuItem } from "@mui/material";
import { map } from "effect/Array";
import { flatMap } from "effect/Either";
import { flow } from "effect/Function";
import { format } from "effect/Inspectable";
import { max } from "effect/Number";
import * as React from "react";
import { useCallback } from "react";
import { useRecoilState } from "recoil";

import { modifyTreasury } from "../models/homeland";
import {
  statusData,
  type StatusMarker,
  type StatusType,
  statusTypes,
} from "../models/status";
import type { AreaName, PowerName } from "../models/types";
import {
  enoughMoney,
  modifyHomeland,
  stockNonEmpty,
  upgradeMarker,
} from "../models/worldState";
import { doModifyWorldFinish, mainSate } from "../store/main";

const costDiff = (current: StatusType, next: StatusType): number =>
  max(0, statusData[next].price - statusData[current].price);

interface IChangeMarkerProperties {
  anchorEl: null | HTMLElement;
  onClose: () => void;
  areaName: AreaName;
  powerName: PowerName;
  statusMarker: StatusMarker;
}

const ChangeMarkerMenu: React.FC<IChangeMarkerProperties> = (properties) => {
  const { areaName, powerName, statusMarker, onClose, anchorEl } = properties;
  const [{ worldState: world }, setMain] = useRecoilState(mainSate);
  const changeMarker = useCallback(
    (
      areaName: AreaName,
      powerName: PowerName,
      oldMarker: StatusMarker,
      newMarker: StatusMarker,
    ): void => {
      setMain(
        doModifyWorldFinish({
          modifier: flow(
            modifyHomeland(
              modifyTreasury(
                -costDiff(oldMarker.type, newMarker.type),
                "upgrade",
              ),
            )(powerName),
            flatMap(upgradeMarker(powerName, areaName, oldMarker, newMarker)),
          ),
          message: `change marker ${areaName}:${powerName}:${format(
            oldMarker,
          )}:${format(newMarker)}`,
        }),
      );
    },
    [setMain],
  );
  const onClick = React.useCallback(
    (statusType: StatusType) => () => {
      onClose();
      changeMarker(
        areaName,
        powerName,
        statusMarker,
        statusData[statusType].defaultMarker,
      );
    },
    [areaName, powerName, statusMarker, changeMarker, onClose],
  );

  return (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      keepMounted={true}
      open={Boolean(anchorEl)}
      onClose={onClose}
    >
      {map((type: StatusType) => (
        <MenuItem
          key={type}
          onClick={onClick(type)}
          disabled={
            type === properties.statusMarker.type ||
            !stockNonEmpty(statusData[type].defaultMarker.shape)(powerName)(
              world,
            ) ||
            !enoughMoney(costDiff(properties.statusMarker.type, type))(
              powerName,
            )(world)
          }
          selected={type === properties.statusMarker.type}
        >
          {statusData[type].text} (£
          {costDiff(properties.statusMarker.type, type)})
        </MenuItem>
      ))(statusTypes)}
    </Menu>
  );
};

export default ChangeMarkerMenu;
