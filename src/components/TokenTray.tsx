import { Schema } from "@effect/schema";
import { MenuItem, Select } from "@mui/material";
import type { SelectChangeEvent } from "@mui/material/Select/SelectInput";
import { Either } from "effect";
import { map } from "effect/Array";
import { struct } from "effect/Data";
import {
  flatMap,
  getOrElse,
  getOrThrowWith,
  isRight,
  match,
  right,
} from "effect/Either";
import { constant, constFalse, flow, pipe } from "effect/Function";
import * as React from "react";
import { useRecoilValue } from "recoil";

import { powerToClassName } from "../misc/database";
import { powerNameSchema } from "../misc/validator.ts";
import { getTreasuryTotal, modifyTreasury } from "../models/homeland";
import { statusData, type StatusType, statusTypes } from "../models/status";
import { stock } from "../models/stock";
import { type PowerName, powerNames } from "../models/types";
import {
  unitPrice,
  type UnitStrength,
  unitStrengths,
  type UnitType,
  unitTypes,
} from "../models/unitType";
import {
  getHomeland,
  getStockCanal,
  homelandGetMoney,
  modifyHomeland,
  modifyStock,
  stockGetMerchant,
  stockGetStatusMarker,
  stockGetUnit,
  stockModifyStatusMarker,
  stockModifyUnit,
} from "../models/worldState";
import { mainSate } from "../store/main.ts";
import CanalToken from "./token/CanalToken";
import MerchantToken from "./token/MerchantToken";
import StatusToken from "./token/StatusToken";
import UnitToken from "./token/UnitToken";

const TokenTray: React.FC = () => {
  const { worldState: world, purchase } = useRecoilValue(mainSate);

  const [power, setPower] = React.useState<PowerName>("Great Britain");
  const onChange = (event: SelectChangeEvent<PowerName>): void => {
    const r = Schema.decodeUnknownEither(powerNameSchema)(event.target.value);
    if (isRight(r)) {
      setPower(r.right);
    }
  };
  const canTake = (
    a: React.JSX.Element,
    left_: Either.Either<number, Error>,
    price: number,
  ): React.JSX.Element | "" =>
    pipe(
      Either.all([left_, getHomeland(getTreasuryTotal)(power)(world)]),
      Either.map(([l, t]: [number, number]) =>
        l > 0 && (!purchase || t >= price) ? (
          <span key={l}>
            {a}x{l}
          </span>
        ) : (
          ""
        ),
      ),
      getOrThrowWith((e: Error) => {
        throw e;
      }),
    );

  return (
    <div className="token-tray">
      <Select
        value={power}
        onChange={onChange}
        className={powerToClassName(power)}
      >
        {map(powerNames, (p) => (
          <MenuItem key={p} value={p} className={powerToClassName(p)}>
            <span className="menu-label">
              {p}
              {purchase
                ? `(£${getOrElse(homelandGetMoney(p)(world), constant(0))})`
                : ""}
            </span>
          </MenuItem>
        ))}
      </Select>
      {map((s: StatusType) => (
        <span key={s}>
          {canTake(
            <span>
              <StatusToken
                type="StockStatus"
                powerName={power}
                statusMarker={statusData[s].defaultMarker}
                send={flow(
                  purchase
                    ? modifyHomeland(
                        modifyTreasury(-statusData[s].price, `購入: ${s}`),
                      )(power)
                    : right,
                  flatMap(
                    stockModifyStatusMarker(
                      statusData[s].defaultMarker.shape,
                      -1,
                    )(power),
                  ),
                )}
              />
              {purchase ? `(£${statusData[s].price})` : ""}
            </span>,
            stockGetStatusMarker(statusData[s].defaultMarker.shape)(power)(
              world,
            ),
            statusData[s].price,
          )}
        </span>
      ))(statusTypes)}
      {map((t: UnitType) =>
        map((s: UnitStrength) => (
          <span key={`${t}${s}`}>
            {canTake(
              <span>
                <UnitToken
                  type="StockUnit"
                  areaUnit={struct({ power, type: t, strength: s })}
                  send={flow(
                    purchase
                      ? modifyHomeland(
                          modifyTreasury(-unitPrice(s), `購入: ${s}`),
                        )(power)
                      : right,
                    flatMap(stockModifyUnit(s, -1)(power)),
                  )}
                />
                {purchase ? `(£${unitPrice(s)})` : ""}
              </span>,
              stockGetUnit(s)(power)(world),
              unitPrice(s),
            )}
          </span>
        ))(unitStrengths),
      )(unitTypes)}
      {canTake(
        <MerchantToken
          powerName={power}
          send={modifyStock(stock.modifyMerchant(-1))(power)}
          type="StockMerchant"
        />,
        stockGetMerchant(power)(world),
        0,
      )}
      {purchase &&
      getStockCanal(world) > 0 &&
      match(getHomeland(getTreasuryTotal)(power)(world), {
        onLeft: constFalse,
        onRight: (t: number) => t >= 30,
      }) ? (
        <CanalToken powerName={power} left={getStockCanal(world)} />
      ) : (
        ""
      )}
    </div>
  );
};

export default TokenTray;
