import { Snackbar, SnackbarContent } from "@mui/material";
import { match, none } from "effect/Option";
import { evolve } from "effect/Struct";
import * as React from "react";
import { useRecoilState } from "recoil";

import type { LogMessage } from "../misc/logMessage";
import { mainSate } from "../store/main";

const ErrorNotification = (): React.JSX.Element => {
  const [main, setMain] = useRecoilState(mainSate);

  const snackbarClose = (): void => {
    setMain(evolve(main, { message: none<LogMessage> }));
  };

  return match(main.message, {
    onSome: (log: LogMessage) => (
      <Snackbar open={true} autoHideDuration={3000} onClose={snackbarClose}>
        <SnackbarContent message={log.body} className={`log-${log.level}`} />
      </Snackbar>
    ),
    onNone: () => <React.Fragment />,
  });
};

export default ErrorNotification;
