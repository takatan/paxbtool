import { Grid } from "@mui/material";
import { Array, HashMap } from "effect";
import { flatten, map, range } from "effect/Array";
import { struct } from "effect/Data";
import { pipe } from "effect/Function";
import { values } from "effect/HashMap";
import { format } from "effect/Inspectable";
import type * as React from "react";
import { useDrop } from "react-dnd";
import { useSetRecoilState } from "recoil";

import { doSendRecv, type IDndStockUnit, type IDndUnit } from "../../misc/dnd";
import { type Homeland, modifyUnit } from "../../models/homeland";
import type { HomelandPowerName } from "../../models/types";
import type { UnitCounter } from "../../models/unitType";
import { modifyHomeland } from "../../models/worldState";
import { mainSate } from "../../store/main.ts";
import UnitToken from "../token/UnitToken";

interface IHomelandBoxProperties {
  powerName: HomelandPowerName;
  homeland: Homeland;
}

const HomelandBox: React.FC<IHomelandBoxProperties> = (properties) => {
  const setMain = useSetRecoilState(mainSate);
  const { homeland, powerName } = properties;
  const [{ canDrop, isOver }, drop] = useDrop<
    IDndUnit | IDndStockUnit,
    unknown,
    { isOver: boolean; canDrop: boolean }
  >({
    accept: ["Unit", "StockUnit"],
    drop: (item) => {
      switch (item.type) {
        case "Unit":
        case "StockUnit": {
          setMain(
            doSendRecv(
              item.send,
              modifyHomeland(
                modifyUnit(
                  struct({
                    type: item.areaUnit.type,
                    strength: item.areaUnit.strength,
                  }),
                  1,
                ),
              )(item.areaUnit.power),
              `Homeland:${powerName}:${item.type}:${format(item.areaUnit)}`,
            ),
          );
          return;
        }
        default: {
          throw new Error(item satisfies never);
        }
      }
    },
    canDrop: (item) =>
      item.areaUnit.power === powerName && !(item.areaUnit.type === "Fleet"),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  let dndClassName = "";
  const isActive = canDrop && isOver;
  if (isActive) {
    dndClassName = "dnd-active";
  } else if (canDrop) {
    dndClassName = "dnd-accept";
  }

  return (
    <Grid item={true} ref={drop} xs={true}>
      <div className={dndClassName}>
        <div className="label">{powerName}</div>
        {pipe(
          homeland.units,
          HashMap.map((a: number, k: UnitCounter) =>
            map(range(1, a), (index: number) => (
              <UnitToken
                type="Unit"
                key={`${format(k)}${index}`}
                areaUnit={struct({
                  power: powerName,
                  type: k.type,
                  strength: k.strength,
                })}
                send={modifyHomeland(
                  modifyUnit(
                    struct({
                      type: k.type,
                      strength: k.strength,
                    }),
                    -1,
                  ),
                )(powerName)}
              />
            )),
          ),
          values,
          Array.fromIterable,
          flatten,
        )}
      </div>
    </Grid>
  );
};
export default HomelandBox;
