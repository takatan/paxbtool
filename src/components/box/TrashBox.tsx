import { Delete } from "@mui/icons-material";
import { format } from "effect/Inspectable";
import type * as React from "react";
import { useDrop } from "react-dnd";
import { useSetRecoilState } from "recoil";

import {
  doSendRecv,
  type IDndMerchant,
  type IDndStatus,
  type IDndUnit,
} from "../../misc/dnd";
import { stock } from "../../models/stock";
import {
  modifyStock,
  stockModifyStatusMarker,
  stockModifyUnit,
} from "../../models/worldState";
import { mainSate } from "../../store/main.ts";

const TrashBox: React.FC = () => {
  const setMain = useSetRecoilState(mainSate);

  const [{ canDrop, isOver }, drop] = useDrop<
    IDndMerchant | IDndUnit | IDndStatus,
    unknown,
    { isOver: boolean; canDrop: boolean }
  >({
    accept: ["Unit", "Merchant", "Status"],
    drop: (item) => {
      switch (item.type) {
        case "Merchant": {
          setMain(
            doSendRecv(
              item.send,
              modifyStock(stock.modifyMerchant(1))(item.powerName),
              `Trash:${item.type}:${item.powerName}`,
            ),
          );
          return;
        }
        case "Unit": {
          setMain(
            doSendRecv(
              item.send,
              stockModifyUnit(item.areaUnit.strength, 1)(item.areaUnit.power),
              `Trash:${item.type}:${format(item.areaUnit)}`,
            ),
          );
          return;
        }
        case "Status": {
          setMain(
            doSendRecv(
              item.send,
              stockModifyStatusMarker(
                item.statusMarker.shape,
                1,
              )(item.powerName),
              `Trash:${item.type}:${format(item.statusMarker)}`,
            ),
          );
          return;
        }
        default: {
          throw new Error(item satisfies never);
        }
      }
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  let dndClassName = "";
  const isActive = canDrop && isOver;
  if (isActive) {
    dndClassName = "dnd-active";
  } else if (canDrop) {
    dndClassName = "dnd-accept";
  }

  return (
    <div ref={drop} className="trash">
      <div className={dndClassName}>
        <div className="center">
          <Delete />
        </div>
      </div>
    </div>
  );
};
export default TrashBox;
