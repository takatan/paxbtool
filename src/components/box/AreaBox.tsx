import { Popover, Switch } from "@mui/material";
import { Array, HashMap, Record } from "effect";
import { flatten, map, replicate } from "effect/Array";
import { right } from "effect/Either";
import { flow, pipe } from "effect/Function";
import { values } from "effect/HashMap";
import { format } from "effect/Inspectable";
import * as React from "react";
import { useCallback } from "react";
import { useDrag, useDrop } from "react-dnd";
import { useRecoilState } from "recoil";

import {
  areaDataRecord,
  type SeaData,
  seaDataRecord,
} from "../../misc/database";
import {
  doSendRecv,
  type IDndAreaBox,
  type IDndCanal,
  type IDndStockStatus,
  type IDndStockUnit,
  type IDndUnit,
} from "../../misc/dnd";
import useWindowDimensions from "../../misc/reactUse";
import { dominionArea, stateArea } from "../../misc/validator";
import {
  addAreaUnit,
  addChangedMarker,
  addMarker,
  type Area,
  isCasusBelli,
  isWarnArea,
  removeAreaUnit,
  setCanal,
} from "../../models/area";
import {
  dominionMarker,
  stateMarker,
  type StatusMarker,
} from "../../models/status";
import type { AreaName, PowerName, SeaName } from "../../models/types";
import type { AreaUnit } from "../../models/unitType";
import { areaSetUnrest, modifyArea } from "../../models/worldState";
import {
  doModifyAreaPosition,
  doModifyWorldFinish,
  mainSate,
} from "../../store/main";
import AreaStatusToken from "../token/AreaStatusToken";
import ChangedStatusToken from "../token/ChangedStatusToken";
import UnitToken from "../token/UnitToken";

const abbrToSeaName: Record<string, SeaName> = Record.mapEntries(
  seaDataRecord,
  (a: SeaData, k: SeaName) => [a.abbreviation, k],
);

interface IAreaBoxProperties {
  areaName: AreaName;
  area: Area;
}

const AreaBox: React.FC<IAreaBoxProperties> = ({ areaName, area }) => {
  const [
    {
      purchase,
      position: { areaPosition },
    },
    setMain,
  ] = useRecoilState(mainSate);
  const setUnrest = useCallback(
    (areaName: AreaName, value: boolean): void => {
      setMain(
        doModifyWorldFinish({
          modifier: areaSetUnrest(value)(areaName),
          message: `doSetUnrestArea ${areaName}:${value.toString()}`,
        }),
      );
    },
    [setMain],
  );
  const { width, height } = useWindowDimensions();
  // TODO どこかにまとめる
  const winSize = { width, height: height * 0.82 };

  const [, drag] = useDrag<IDndAreaBox, never, never>({
    type: "AreaBox",
    item: { type: "AreaBox" },
    end: (_dropResult, monitor) => {
      const d = monitor.getDifferenceFromInitialOffset();
      if (d !== null) {
        setMain(
          doModifyAreaPosition({
            areaName,
            position: {
              top: Number.parseFloat(
                (
                  areaPosition[areaName].top +
                  d.y / (winSize.height / 100)
                ).toFixed(1),
              ),
              left: Number.parseFloat(
                (
                  areaPosition[areaName].left +
                  d.x / (winSize.width / 100)
                ).toFixed(1),
              ),
            },
          }),
        );
      }
    },
  });

  const [{ canDrop, isOver }, drop] = useDrop<
    IDndUnit | IDndStockStatus | IDndStockUnit | IDndCanal,
    unknown,
    { isOver: boolean; canDrop: boolean }
  >({
    accept: ["Unit", "StockUnit", "StockStatus", "Canal"],
    canDrop: (item) =>
      !(
        (item.type === "Canal" &&
          !(areaName === "Panama" || areaName === "Central America")) ||
        area.canal ||
        (item.type === "StockStatus" &&
          item.statusMarker === dominionMarker &&
          !Array.contains(dominionArea, areaName)) ||
        (item.type === "StockStatus" &&
          item.statusMarker === stateMarker &&
          !Array.contains(stateArea, areaName))
      ),
    drop: (item) => {
      switch (item.type) {
        case "Unit":
        case "StockUnit": {
          setMain(
            doSendRecv(
              item.send,
              modifyArea(flow(addAreaUnit(item.areaUnit), right))(areaName),
              `${areaName}:${item.type}:${format(item.areaUnit)}`,
            ),
          );
          return;
        }
        case "StockStatus": {
          setMain(
            doSendRecv(
              item.send,
              purchase
                ? modifyArea(
                    addChangedMarker(item.powerName, item.statusMarker),
                  )(areaName)
                : modifyArea(addMarker(item.powerName, item.statusMarker))(
                    areaName,
                  ),
              `${areaName}:${item.type}:${item.powerName}:${format(
                item.statusMarker,
              )}`,
            ),
          );
          return;
        }
        case "Canal": {
          setMain(
            doSendRecv(
              item.send,
              modifyArea(setCanal(true, item.powerName))(areaName),
              `${areaName}:${item.type}:${item.powerName}`,
            ),
          );
          return;
        }
        default: {
          throw new Error(item satisfies never);
        }
      }
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });
  let dndClassName = "";
  const isActive = canDrop && isOver;
  if (isActive) {
    dndClassName = "dnd-active";
  } else if (canDrop) {
    dndClassName = "dnd-accept";
  }

  const [anchorElement, setAnchorElement] =
    React.useState<HTMLDivElement | null>(null);
  const onClick = React.useCallback(
    (event: React.MouseEvent<HTMLDivElement>) => {
      setAnchorElement(event.currentTarget);
    },
    [],
  );
  const onUnrestCheck = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setUnrest(areaName, event.target.checked);
    },
    [areaName, setUnrest],
  );
  const onClose = React.useCallback(() => {
    setAnchorElement(null);
  }, []);
  return (
    <div
      ref={drag}
      id={`area-${areaDataRecord[areaName].abbreviation}`}
      className={[
        "areabox",
        `category-${areaDataRecord[areaName].category}`,
      ].join(" ")}
      style={{
        position: "absolute",
        top: `${areaPosition[areaName].top}%`,
        left: `${areaPosition[areaName].left}%`,
      }}
    >
      <div className={dndClassName} ref={drop}>
        <div
          onClick={onClick}
          className={["clickable", isWarnArea(area) ? "warning" : ""].join(" ")}
        >
          {areaName}/£{areaDataRecord[areaName].ev}(
          {areaDataRecord[areaName].cs})
          {pipe(
            areaDataRecord[areaName].adjacencies,
            Array.filterMap((adj: string) => Record.get(abbrToSeaName, adj)),
            map((s: SeaName) => (
              <span
                key={s}
                className={`sea-marker-box sea-color-${seaDataRecord[
                  s
                ].abbreviation.replaceAll(".", "")}`}
              />
            )),
          )}
        </div>
        <Popover
          open={Boolean(anchorElement)}
          anchorEl={anchorElement}
          onClose={onClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          暴動
          <Switch checked={area.unrest} onChange={onUnrestCheck} />
        </Popover>
        <div>
          {area.canal ? <span className="canal">運</span> : ""}
          {area.unrest ? <span className="unrest">暴</span> : ""}
          {isCasusBelli(area) ? <span className="casus-belli">宣</span> : ""}
          <span>
            {pipe(
              area.markers,
              HashMap.map((a1: StatusMarker, k: PowerName) => (
                <AreaStatusToken
                  key={k}
                  areaName={areaName}
                  powerName={k}
                  statusMarker={a1}
                  purchase={purchase}
                />
              )),
              HashMap.values,
            )}
          </span>
          <span className="changed-marker">
            {pipe(
              area.changedMarkers,
              HashMap.map((a1: StatusMarker, k: PowerName) => (
                <ChangedStatusToken
                  key={k}
                  areaName={areaName}
                  powerName={k}
                  statusMarker={a1}
                />
              )),
              values,
            )}
          </span>
          <span>
            {pipe(
              area.units,
              HashMap.map((number_: number, k: AreaUnit) =>
                replicate(k, number_),
              ),
              values,
              Array.fromIterable,
              flatten,
              map((k: AreaUnit, index: number) => (
                <UnitToken
                  type="Unit"
                  key={index}
                  areaUnit={k}
                  send={modifyArea(removeAreaUnit(k))(areaName)}
                />
              )),
            )}
            <br />
          </span>
        </div>
      </div>
    </div>
  );
};
export default AreaBox;
