import { Tooltip } from "@mui/material";
import { Array, HashMap, HashSet } from "effect";
import { flatten, map, replicate } from "effect/Array";
import { right } from "effect/Either";
import { flow, pipe } from "effect/Function";
import { format } from "effect/Inspectable";
import type * as React from "react";
import { useDrag, useDrop } from "react-dnd";
import { useRecoilState } from "recoil";

import { seaDataRecord } from "../../misc/database";
import {
  doSendRecv,
  type IDndAreaBox,
  type IDndMerchant,
  type IDndStockMerchant,
  type IDndStockUnit,
  type IDndUnit,
} from "../../misc/dnd";
import useWindowDimensions from "../../misc/reactUse";
import {
  addMerchant,
  addSeaUnit,
  removeMerchant,
  removeSeaUnit,
  type SeaZone,
} from "../../models/sea";
import type { PowerName, SeaName } from "../../models/types";
import type { AreaUnit } from "../../models/unitType";
import { modifySeaZone } from "../../models/worldState";
import { doModifySeaPosition, mainSate } from "../../store/main";
import MerchantToken from "../token/MerchantToken";
import UnitToken from "../token/UnitToken";

interface ISeaZoneBoxProperties {
  seaName: SeaName;
  seaZone: SeaZone;
}

const SeaZoneBox: React.FC<ISeaZoneBoxProperties> = (properties) => {
  const { seaName, seaZone } = properties;
  const [
    {
      position: { seaPosition },
    },
    setMain,
  ] = useRecoilState(mainSate);
  const { width, height } = useWindowDimensions();
  // TODO どこかにまとめる
  const winSize = { width, height: height * 0.82 };

  const [, drag] = useDrag<IDndAreaBox>({
    type: "AreaBox",
    item: { type: "AreaBox" },
    end: (_dropResult, monitor) => {
      const d = monitor.getDifferenceFromInitialOffset();
      if (d !== null) {
        setMain(
          doModifySeaPosition({
            seaName,
            position: {
              top: Number.parseFloat(
                (
                  seaPosition[seaName].top +
                  d.y / (winSize.height / 100)
                ).toFixed(1),
              ),
              left: Number.parseFloat(
                (
                  seaPosition[seaName].left +
                  d.x / (winSize.width / 100)
                ).toFixed(1),
              ),
            },
          }),
        );
      }
    },
  });

  const [{ canDrop, isOver }, drop] = useDrop<
    IDndMerchant | IDndUnit | IDndStockMerchant | IDndStockUnit,
    unknown,
    { canDrop: boolean; isOver: boolean }
  >({
    accept: ["Unit", "StockUnit", "Merchant", "StockMerchant"],
    // 戦争中は海域に置くこともある
    // canDrop: item => {
    //   return !(
    //     (item.type === "Unit" || item.type === "StockUnit") &&
    //     item.areaUnit.type === "Army"
    //   );
    // },
    drop: (item) => {
      switch (item.type) {
        case "StockMerchant":
        case "Merchant": {
          setMain(
            doSendRecv(
              item.send,
              modifySeaZone(addMerchant(item.powerName))(seaName),
              `${seaName}:${item.type}:${item.powerName}`,
            ),
          );
          return;
        }
        case "Unit":
        case "StockUnit": {
          setMain(
            doSendRecv(
              item.send,
              modifySeaZone(flow(addSeaUnit(item.areaUnit), right))(seaName),
              `${seaName}:${item.type}:${format(item.areaUnit)}`,
            ),
          );
          return;
        }
        default: {
          throw new Error(item satisfies never);
        }
      }
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  let dndClassName = "";
  const isActive = canDrop && isOver;
  if (isActive) {
    dndClassName = "dnd-active";
  } else if (canDrop) {
    dndClassName = "dnd-accept";
  }

  return (
    <div
      id={`sea-${seaDataRecord[seaName].abbreviation.replaceAll(".", "")}`}
      ref={drag}
      className={[
        "seazone",
        `sea-color-${seaDataRecord[seaName].abbreviation.replaceAll(".", "")}`,
      ].join(" ")}
      style={{
        position: "absolute",
        top: `${seaPosition[seaName].top}%`,
        left: `${seaPosition[seaName].left}%`,
      }}
    >
      <div className={dndClassName} ref={drop}>
        <Tooltip title={seaDataRecord[seaName].adjacencies.join(", ")}>
          <div className={`label`}>{seaName}</div>
        </Tooltip>
        {pipe(
          HashSet.values(seaZone.merchants),
          Array.fromIterable,
          map((p: PowerName) => (
            <MerchantToken
              key={p}
              powerName={p}
              send={modifySeaZone(removeMerchant(p))(seaName)}
              type="Merchant"
            />
          )),
        )}

        {pipe(
          seaZone.units,
          HashMap.map((number_: number, k: AreaUnit) => replicate(k, number_)),
          HashMap.values,
          Array.fromIterable,
          flatten,
          map((k: AreaUnit, index: number) => (
            <UnitToken
              type="Unit"
              key={index}
              send={modifySeaZone(removeSeaUnit(k))(seaName)}
              areaUnit={k}
            />
          )),
        )}
      </div>
    </div>
  );
};
export default SeaZoneBox;
