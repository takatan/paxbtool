import { AppBar, Toolbar } from "@mui/material";
import type * as React from "react";
import { useRecoilValue } from "recoil";

import { mainSate } from "../store/main";
import TrashBox from "./box/TrashBox";
import PurchaseSwitch from "./PurchseSwitch";
import TokenTray from "./TokenTray";

const PBAppBar: React.FC = () => {
  const turn = useRecoilValue(mainSate).checkpoints.length;

  return (
    <AppBar position="fixed" className="pbappbar">
      <Toolbar variant="dense">
        <div className="turn">
          {1880 + turn * 4}年(ターン{turn + 1})
        </div>
        <TrashBox />
        <TokenTray />
        <PurchaseSwitch />
      </Toolbar>
    </AppBar>
  );
};

export default PBAppBar;
