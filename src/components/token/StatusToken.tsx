import { Tooltip } from "@mui/material";
import type { Either } from "effect/Either";
import type * as React from "react";
import { useDrag } from "react-dnd";

import { powerToClassName } from "../../misc/database";
import type { IDndStatus, IDndStockStatus } from "../../misc/dnd";
import { statusData, type StatusMarker } from "../../models/status";
import type { PowerName } from "../../models/types";
import type { WorldState } from "../../models/worldState";

interface IStatusTokenState {
  send: (w: WorldState) => Either<WorldState, Error>;
  powerName: PowerName;
  statusMarker: StatusMarker;
  type: "Status" | "StockStatus";
}

type StatusTokenProperties = IStatusTokenState;
const StatusToken: React.FC<StatusTokenProperties> = (properties) => {
  const { send, type, powerName, statusMarker } = properties;
  const [{ isDragging }, drag] = useDrag<
    IDndStatus | IDndStockStatus,
    never,
    { isDragging: boolean }
  >({
    type,
    item: {
      type,
      send,
      statusMarker,
      powerName,
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? "opacity" : "";

  return (
    <Tooltip title={`${powerName}:${statusData[statusMarker.type].text}`}>
      <span
        ref={drag}
        className={`status-token ${powerToClassName(
          properties.powerName,
        )} ${opacity}`}
      >
        <span className="label">
          {/* {powerDataRecord[props.powerName].abbr} */}{" "}
          {statusData[properties.statusMarker.type].text[0]}
        </span>
      </span>
    </Tooltip>
  );
};
export default StatusToken;
