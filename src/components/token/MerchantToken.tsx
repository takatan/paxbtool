import { Tooltip } from "@mui/material";
import type { Either } from "effect/Either";
import type * as React from "react";
import { useDrag } from "react-dnd";

import { powerToClassName } from "../../misc/database";
import type { IDndMerchant, IDndStockMerchant } from "../../misc/dnd";
import type { PowerName } from "../../models/types";
import type { WorldState } from "../../models/worldState";

interface IMerchantTokenState {
  send: (w: WorldState) => Either<WorldState, Error>;
  powerName: PowerName;
  type: "Merchant" | "StockMerchant";
}

type IMerchantTokenProperties = IMerchantTokenState;

const MerchantToken: React.FC<IMerchantTokenProperties> = (properties) => {
  const { type, send, powerName } = properties;
  const [{ isDragging }, drag] = useDrag<
    IDndMerchant | IDndStockMerchant,
    never,
    { isDragging: boolean }
  >({
    type,
    item: {
      type,
      send,
      powerName,
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? "opacity" : "";

  return (
    <Tooltip title={`${powerName}:商船`}>
      <span
        ref={drag}
        className={`merchant-token ${powerToClassName(
          properties.powerName,
        )} ${opacity}`}
      >
        <span className="label">
          {" "}
          {/* {powerDataRecord[props.powerName].abbr} */}商
        </span>
      </span>
    </Tooltip>
  );
};

export default MerchantToken;
