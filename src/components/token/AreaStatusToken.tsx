import React from "react";

import { removeMarker } from "../../models/area";
import type { StatusMarker } from "../../models/status";
import type { AreaName, PowerName } from "../../models/types";
import { modifyArea } from "../../models/worldState";
import ChangeMarkerMenu from "../ChangeMarkerMenu";
import StatusToken from "./StatusToken";

interface IStatusTokenState {
  areaName: AreaName;
  powerName: PowerName;
  statusMarker: StatusMarker;
  purchase: boolean;
}
type IStatusTokenProperties = IStatusTokenState;

// Menuが必要なのでラップする意味がある
const AreaStatusToken: React.FC<IStatusTokenProperties> = (properties) => {
  const { purchase, areaName, powerName, statusMarker } = properties;
  const [anchorElement, setAnchorElement] = React.useState<null | HTMLElement>(null);

  const onClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorElement(event.currentTarget);
    },
    [],
  );
  const onClose = React.useCallback(() => {
    setAnchorElement(null);
  }, []);
  return (
    <span>
      <span onClick={onClick}>
        <StatusToken
          type="Status"
          send={modifyArea(removeMarker(powerName, statusMarker))(areaName)}
          powerName={powerName}
          statusMarker={statusMarker}
        />
      </span>
      {purchase && Boolean(anchorElement) ? (
        <ChangeMarkerMenu
          powerName={powerName}
          areaName={areaName}
          statusMarker={statusMarker}
          anchorEl={anchorElement}
          onClose={onClose}
        />
      ) : (
        ""
      )}
    </span>
  );
};

export default AreaStatusToken;
