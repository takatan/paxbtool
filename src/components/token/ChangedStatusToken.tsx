import { Button, Popover } from "@mui/material";
import { flatMap } from "effect/Either";
import { flow } from "effect/Function";
import * as React from "react";
import { useSetRecoilState } from "recoil";

import { addMarker, removeChangedMarker } from "../../models/area";
import type { StatusMarker } from "../../models/status";
import type { AreaName, PowerName } from "../../models/types";
import { modifyArea } from "../../models/worldState";
import { doModifyWorldFinish, mainSate } from "../../store/main";
import StatusToken from "./StatusToken";

interface IChangedStatusMarkerTokenProperties {
  areaName: AreaName;
  powerName: PowerName;
  statusMarker: StatusMarker;
}

// Menuが必要なのでラップする意味がある
const ChangedStatusToken: React.FC<IChangedStatusMarkerTokenProperties> = (
  properties,
) => {
  const setMain = useSetRecoilState(mainSate);
  const { areaName, powerName, statusMarker } = properties;
  const [anchorElement, setAnchorElement] = React.useState<null | HTMLElement>(null);

  const onClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorElement(event.currentTarget);
    },
    [],
  );
  const onClose = React.useCallback(() => {
    setAnchorElement(null);
  }, []);
  const onButtonClick = React.useCallback(() => {
    onClose();
    setMain(
      doModifyWorldFinish({
        modifier: flow(
          modifyArea(removeChangedMarker(powerName, statusMarker))(areaName),
          flatMap(modifyArea(addMarker(powerName, statusMarker))(areaName)),
        ),
        message: "confirm marker",
      }),
    );
  }, [onClose, setMain, powerName, statusMarker, areaName]);
  return (
    <span>
      <span onClick={onClick}>
        <StatusToken
          type="Status"
          send={modifyArea(removeChangedMarker(powerName, statusMarker))(
            areaName,
          )}
          powerName={powerName}
          statusMarker={statusMarker}
        />
      </span>
      <Popover open={Boolean(anchorElement)} anchorEl={anchorElement} onClose={onClose}>
        <Button variant={"contained"} color="info" onClick={onButtonClick}>
          確定
        </Button>
      </Popover>
    </span>
  );
};
export default ChangedStatusToken;
