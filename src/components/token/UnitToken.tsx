import { Tooltip } from "@mui/material";
import type { Either } from "effect/Either";
import type * as React from "react";
import { useDrag } from "react-dnd";

import { powerToClassName } from "../../misc/database";
import type { IDndStockUnit, IDndUnit } from "../../misc/dnd";
import type { AreaUnit } from "../../models/unitType";
import type { WorldState } from "../../models/worldState";

interface IUnitTokenState {
  send: (w: WorldState) => Either<WorldState, Error>;
  areaUnit: AreaUnit;
  type: "Unit" | "StockUnit";
}

type UnitTokenProperties = IUnitTokenState;
const UnitToken: React.FC<UnitTokenProperties> = (properties) => {
  const { areaUnit, type, send } = properties;
  const [{ isDragging }, drag] = useDrag<
    IDndUnit | IDndStockUnit,
    never,
    { isDragging: boolean }
  >({
    type,
    item: {
      type,
      send,
      areaUnit,
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? "opacity" : "";

  return (
    <Tooltip title={`${areaUnit.power}:${areaUnit.type}:${areaUnit.strength}`}>
      <span
        ref={drag}
        className={`unit-token ${powerToClassName(areaUnit.power)} ${opacity}`}
      >
        <span className="label">
          {areaUnit.type[0]}
          {areaUnit.strength}
        </span>
      </span>
    </Tooltip>
  );
};
export default UnitToken;
