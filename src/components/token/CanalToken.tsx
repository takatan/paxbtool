import { flatMap, right } from "effect/Either";
import { flow } from "effect/Function";
import type * as React from "react";
import { useDrag } from "react-dnd";

import type { IDndCanal } from "../../misc/dnd";
import { modifyTreasury, modifyVP } from "../../models/homeland";
import type { PowerName } from "../../models/types";
import { modifyHomeland } from "../../models/worldState";

interface ICanalTokenState {
  powerName: PowerName;
  left: number;
}

type ICanalTokenProperties = ICanalTokenState;

const CanalToken: React.FC<ICanalTokenProperties> = (properties) => {
  const { powerName, left } = properties;
  const [{ isDragging }, drag] = useDrag<
    IDndCanal,
    never,
    { isDragging: boolean }
  >({
    type: "Canal",
    item: {
      powerName,
      type: "Canal",
      send: flow(
        left === 2
          ? modifyHomeland(flow(modifyVP(15, "最初の運河建設"), right))(
              powerName,
            )
          : right,
        flatMap(modifyHomeland(modifyTreasury(-30, "運河建設"))(powerName)),
      ),
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? "opacity" : "";

  return (
    <span>
      <span ref={drag} className={["canal", opacity].join(" ")}>
        運河
      </span>
      (£30)x{left}
    </span>
  );
};
export default CanalToken;
