import {
  Add,
  ArrowDownward,
  Check,
  Notes,
  OpenInBrowser,
  SaveAlt,
  Undo,
} from "@mui/icons-material";
import {
  Dialog,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import { head } from "effect/Array";
import { fromNullable } from "effect/Either";
import { constant } from "effect/Function";
import { format } from "effect/Inspectable";
import { match } from "effect/Option";
import * as React from "react";
import { useCallback } from "react";
import { useRecoilState } from "recoil";

import { mainStateToJson } from "../misc/json";
import { addFinalVP } from "../models/worldState";
import {
  doLoadData,
  doModifyWorldFinish,
  doSnackbarFinish,
  doTurnEnd,
  doUndo,
  mainSate,
  type Snapshot,
} from "../store/main";

interface ISideDrawerProperties {
  open: boolean;
  onClose: () => void;
}

const SideDrawer: React.FC<ISideDrawerProperties> = (properties) => {
  const { open, onClose } = properties;
  const [main, setMain] = useRecoilState(mainSate);
  const { areaPosition, seaPosition } = main.position;
  const finalVP = useCallback((): void => {
    setMain(
      doModifyWorldFinish({
        modifier: addFinalVP,
        message: "add final vp",
      }),
    );
  }, [setMain]);

  const [dump, setDump] = React.useState(0);
  const onClickFinalVP = React.useCallback(() => {
    onClose();
    finalVP();
  }, [finalVP, onClose]);
  const onClickUndo = React.useCallback(() => {
    setMain(doUndo());
  }, [setMain]);
  const onClickSave = React.useCallback(() => {
    localStorage.setItem("mainState", format(mainStateToJson(main)));
    setMain(doSnackbarFinish({ body: "保存しました", level: "info" }));
    onClose();
  }, [main, onClose, setMain]);
  const onClickLoad = React.useCallback(() => {
    setMain(
      doLoadData(
        fromNullable(
          localStorage.getItem("mainState"),
          constant(new Error("storage null")),
        ),
      ),
    );
    onClose();
  }, [onClose, setMain]);
  const onClickTurnEnd = React.useCallback(() => {
    onClose();
    setMain(doTurnEnd());
    onClickSave();
  }, [onClose, setMain, onClickSave]);
  const onDumpClick = React.useCallback(
    (n: number) => () => {
      setDump(n);
    },
    [],
  );
  const onDumpClose = React.useCallback(() => {
    setDump(0);
    onClose();
  }, [onClose]);

  return (
    <Drawer open={open} onClose={onClose} className="side-drawer">
      <List>
        <div className="vspace" />
        <Divider />
        <ListItemButton onClick={onClickUndo}>
          <ListItemIcon>
            <Undo />
          </ListItemIcon>

          <ListItemText>
            Undo[{main.snapshots.length}]
            {match(head(main.snapshots), {
              onNone: constant(""),
              onSome: (s: Snapshot) => s.message,
            })}
          </ListItemText>
        </ListItemButton>
        <ListItemButton onClick={onClickTurnEnd}>
          <ListItemIcon>
            <Check />
          </ListItemIcon>
          <ListItemText>ターン終了・記録</ListItemText>
        </ListItemButton>

        <ListItemButton onClick={onClickSave}>
          <ListItemIcon>
            <SaveAlt />
          </ListItemIcon>
          <ListItemText>LocalStorageに保存</ListItemText>
        </ListItemButton>
        <ListItemButton onClick={onClickFinalVP}>
          <ListItemIcon>
            <Add />
          </ListItemIcon>
          <ListItemText>最終VP加算</ListItemText>
        </ListItemButton>
        <ListItemButton onClick={onClickLoad}>
          <ListItemIcon>
            <OpenInBrowser />
          </ListItemIcon>
          <ListItemText>LocalStorageから復元</ListItemText>
        </ListItemButton>
        <ListItem divider={true} />
        <ListItem>Development</ListItem>
        <ListItemButton onClick={onDumpClick(1)}>
          <ListItemIcon>
            <Notes />
          </ListItemIcon>
          <ListItemText>Dump Area Position</ListItemText>
        </ListItemButton>
        <ListItemButton onClick={onDumpClick(2)}>
          <ListItemIcon>
            <Notes />
          </ListItemIcon>
          <ListItemText>Dump Sea Position</ListItemText>
        </ListItemButton>
        <ListItemButton onClick={onDumpClick(3)}>
          <ListItemIcon>
            <ArrowDownward />
          </ListItemIcon>
          <ListItemText>Dump State</ListItemText>
        </ListItemButton>
      </List>
      <Dialog open={dump > 0} onClose={onDumpClose} className="debug-dialog">
        <br />
        <code>
          {dump === 1 && JSON.stringify(areaPosition)}
          {dump === 2 && JSON.stringify(seaPosition)}
          {dump === 3 && JSON.stringify(main)}
        </code>
      </Dialog>
    </Drawer>
  );
};
export default SideDrawer;
