import { Switch } from "@mui/material";
import { constant } from "effect/Function";
import { evolve } from "effect/Struct";
import * as React from "react";
import { useRecoilState } from "recoil";

import { mainSate } from "../store/main";

const PurchaseSwitch: React.FC = () => {
  const [main, setMain] = useRecoilState(mainSate);
  const onChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setMain(evolve(main, { purchase: constant(event.target.checked) }));
    },
    [main, setMain],
  );

  return (
    <span>
      購入
      <Switch checked={main.purchase} onChange={onChange} />
    </span>
  );
};

export default PurchaseSwitch;
