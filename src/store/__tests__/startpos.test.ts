import { describe, expect, test } from "vitest";

import { getTreasuryTotal } from "../../models/homeland";
import { startWorldState } from "../startpos";

describe("test", () => {
  test("testabcd", () => {
    expect(2).toStrictEqual(2);
  });
  test("startpos", () => {
    expect(getTreasuryTotal(startWorldState.homelands["Great Britain"])).toBe(
      37,
    );
  });
});
