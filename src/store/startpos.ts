import * as RA from "effect/Array";
import { flatten, map } from "effect/Array";
import { struct } from "effect/Data";
import { type Either, flatMap, getOrThrow, right } from "effect/Either";
import { constant, flow, pipe } from "effect/Function";
import * as RR from "effect/Record";
import { evolve } from "effect/Struct";

import { modifyTreasury } from "../models/homeland";
import { statusData, type StatusType } from "../models/status";
import {
  type AreaName,
  greatPowerNames,
  type HomelandPowerName,
  type PowerName,
  type SeaName,
} from "../models/types";
import { type UnitAbbr, unitAbbrToUnitCounter } from "../models/unitType";
import {
  modifyHomeland,
  moveMarkerStockToArea,
  moveMerchantStockToSea,
  moveUnitStockToArea,
  moveUnitStockToHome,
  moveUnitStockToSea,
  totalBalance,
  type WorldModifier,
  type WorldState,
} from "../models/worldState";
import { initWorldState } from "./default";

interface IPowerData {
  merchants: SeaName[];
  homeUnits: UnitAbbr[];
  areas: IAreaData[];
  seas: ISeaData[];
}

interface IAreaData {
  name: AreaName;
  marker: StatusType;
  units: UnitAbbr[];
}
interface ISeaData {
  name: SeaName;
  units: UnitAbbr[];
}

const startPosData: Record<PowerName, IPowerData> = {
  "Chinese Empire": {
    areas: [],
    seas: [],
    merchants: [],
    homeUnits: [],
  },
  "Ottoman Empire": {
    areas: [],
    seas: [],
    merchants: [],
    homeUnits: [],
  },
  "Great Britain": {
    merchants: [
      "North Atlantic Ocean",
      "South Atlantic Ocean",
      "Mediterranean",
      "Indian Ocean",
      "Oceania",
      "North China Sea",
      "South China Sea",
    ],
    homeUnits: ["A3", "A1", "A1", "A1"],
    seas: [{ name: "North Atlantic Ocean", units: ["F10", "F10", "F10"] }],
    areas: [
      { name: "Australia", marker: "Possession", units: ["F3", "A10"] },
      { name: "Bengal", marker: "Possession", units: ["F1", "A10"] },
      { name: "Canada", marker: "Dominion", units: ["F3", "A10"] },
      { name: "Cape Colony", marker: "Possession", units: ["F1", "A3"] },
      { name: "Federated Malay States", marker: "Possession", units: ["A1"] },
      { name: "Fiji", marker: "Possession", units: ["A1"] },
      { name: "Gold Coast", marker: "Possession", units: ["A1"] },
      { name: "Guiana", marker: "Possession", units: ["A1"] },
      { name: "Hindoostan", marker: "Possession", units: ["A1"] },
      { name: "Hong Kong", marker: "Possession", units: ["A1"] },
      { name: "Kashmir", marker: "Possession", units: ["A1"] },
      { name: "Newfoundland", marker: "Possession", units: ["A1"] },
      { name: "New Zealand", marker: "Possession", units: ["A1"] },
      { name: "Punjab", marker: "Possession", units: ["A10"] },
      { name: "Rajputana", marker: "Possession", units: ["A1"] },
      { name: "United Provinces", marker: "Possession", units: ["A10"] },
    ],
  },
  France: {
    merchants: [
      "North Atlantic Ocean",
      "South Atlantic Ocean",
      "Mediterranean",
      "Indian Ocean",
      "South China Sea",
    ],
    homeUnits: ["A3", "A3", "A1"],
    seas: [{ name: "North Atlantic Ocean", units: ["F10", "F10"] }],
    areas: [
      { name: "Algiers", marker: "Possession", units: ["F3", "A10"] },
      { name: "Cochin China", marker: "Possession", units: ["F3", "A3"] },
      { name: "Guiana", marker: "Possession", units: ["A1"] },
      { name: "Senegambia", marker: "Possession", units: ["A1"] },
    ],
  },
  Germany: {
    merchants: [
      "North Atlantic Ocean",
      "South Atlantic Ocean",
      "Mediterranean",
      "Indian Ocean",
      "Oceania",
    ],
    homeUnits: ["A10", "A10", "A3", "A3", "A1", "A1"],
    seas: [{ name: "Baltic Sea", units: ["F10"] }],
    areas: [],
  },
  "Austria-Hungary": {
    merchants: [],
    homeUnits: ["A10", "A10", "A10", "A10"],
    seas: [{ name: "Mediterranean", units: ["F3"] }],
    areas: [],
  },
  Italy: {
    merchants: ["Mediterranean", "Indian Ocean"],
    homeUnits: ["A3", "A1", "A1"],
    seas: [{ name: "Mediterranean", units: ["F10"] }],
    areas: [],
  },
  Japan: {
    merchants: ["North China Sea"],
    homeUnits: ["A10", "A10", "A1", "A1"],
    seas: [{ name: "North China Sea", units: ["F10"] }],
    areas: [],
  },
  Russia: {
    merchants: ["Black Sea"],
    homeUnits: ["A10", "A10", "A10", "A10"],
    seas: [
      { name: "Black Sea", units: ["F1"] },
      { name: "Baltic Sea", units: ["F10"] },
      { name: "North China Sea", units: ["F1"] },
    ],
    areas: [],
  },
  "United States": {
    merchants: ["North Pacific Ocean", "Caribbean Sea"],
    homeUnits: ["A1", "A1"],
    seas: [{ name: "North Atlantic Ocean", units: ["F3"] }],
    areas: [{ name: "Alaska", marker: "Possession", units: ["A1"] }],
  },
  Belgium: {
    merchants: ["North Atlantic Ocean", "South Atlantic Ocean"],
    homeUnits: ["A3", "A1", "A1"],
    seas: [{ name: "North Atlantic Ocean", units: ["F3"] }],
    areas: [{ name: "Kongo", marker: "Influence", units: [] }],
  },
  Netherlands: {
    merchants: [
      "North Atlantic Ocean",
      "South Atlantic Ocean",
      "Indian Ocean",
      "Oceania",
    ],
    homeUnits: ["A3", "A1", "A1"],
    seas: [{ name: "North Atlantic Ocean", units: ["F3"] }],
    areas: [
      { name: "Dutch East Indies", marker: "Possession", units: ["F1", "A3"] },
      { name: "Guiana", marker: "Possession", units: ["A1"] },
    ],
  },
  Spain: {
    merchants: [
      "North Atlantic Ocean",
      "Mediterranean",
      "Indian Ocean",
      "South China Sea",
      // errata
      "South Atlantic Ocean",
      "North Pacific Ocean",
    ],
    homeUnits: ["A3", "A1", "A1"],
    seas: [{ name: "North Atlantic Ocean", units: ["F3"] }],
    areas: [
      { name: "Cuba", marker: "Possession", units: ["F3", "A3"] },
      { name: "Marocco", marker: "Influence", units: [] },
      { name: "Philippines", marker: "Possession", units: ["F3", "A1"] },
      { name: "Porto Rico", marker: "Possession", units: ["A1"] },
      { name: "Rio de Oro", marker: "Possession", units: ["A1"] },
    ],
  },
  Portugal: {
    merchants: ["North Atlantic Ocean", "South Atlantic Ocean", "Indian Ocean"],
    homeUnits: ["A1", "A1"],
    seas: [{ name: "North Atlantic Ocean", units: ["F3"] }],
    areas: [
      { name: "Angola", marker: "Possession", units: ["A1"] },
      { name: "Mozambique", marker: "Possession", units: ["A1"] },
    ],
  },
};

export const initTurn = (
  ws: Either<WorldState, Error>,
): Either<WorldState, Error> =>
  pipe(
    pipe(
      greatPowerNames,
      RA.flatMap((p: HomelandPowerName) => [
        (w: WorldState): Either<WorldState, Error> =>
          modifyHomeland(modifyTreasury(totalBalance(p)(w), "ターン開始時"))(p)(
            w,
          ),
        (w: WorldState): Either<WorldState, Error> =>
          modifyHomeland(flow(evolve({ vpEvents: constant([]) }), right))(p)(w),
        (w: WorldState): Either<WorldState, Error> =>
          modifyHomeland(flow(evolve({ treasuryEvents: constant([]) }), right))(
            p,
          )(w),
      ]),
    ),
    RA.reverse,
    RA.reduce(
      ws,
      (
        a: Either<WorldState, Error>,
        b: (x: WorldState) => Either<WorldState, Error>,
      ) => flatMap(b)(a),
    ),
  );

const setupWorldState = (): WorldState => {
  // 将来的にはvariantも?
  const sp = pipe(
    startPosData,
    RR.collect((p: PowerName, a: IPowerData) => [
      map(
        (ad: IAreaData): WorldModifier =>
          moveMarkerStockToArea(
            p,
            ad.name,
            statusData[ad.marker].defaultMarker,
          ),
      )(a.areas),
      map((s: SeaName): WorldModifier => moveMerchantStockToSea(p, s))(
        a.merchants,
      ),
      RA.flatMap((ad: IAreaData) =>
        map((ua: UnitAbbr): WorldModifier => {
          const uc = unitAbbrToUnitCounter(ua);
          return moveUnitStockToArea(
            struct({ power: p, type: uc.type, strength: uc.strength }),
            ad.name,
          );
        })(ad.units),
      )(a.areas),
      map((ua: UnitAbbr): WorldModifier => {
        const uc = unitAbbrToUnitCounter(ua);
        return moveUnitStockToHome(p, uc.type, uc.strength);
      })(a.homeUnits),
      RA.flatMap((ad: ISeaData) =>
        map((ua: UnitAbbr): WorldModifier => {
          const uc = unitAbbrToUnitCounter(ua);
          return moveUnitStockToSea(
            struct({ power: p, type: uc.type, strength: uc.strength }),
            ad.name,
          );
        })(ad.units),
      )(a.seas),
    ]),
    flatten,
    flatten,
    RA.reduce(
      right(initWorldState()),
      (a: Either<WorldState, Error>, b: WorldModifier) => flatMap(b)(a),
    ),
  );
  const sp2 = initTurn(sp);
  return getOrThrow(sp2);
};

export const startWorldState: WorldState = setupWorldState();
