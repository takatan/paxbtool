import { Schema } from "@effect/schema";
import { Either, HashMap, HashSet, Option } from "effect";
import { append, map as mapA, matchRight } from "effect/Array";
import { flatMap, right } from "effect/Either";
import { constant, pipe } from "effect/Function";
import { none, some } from "effect/Option";
import { map } from "effect/Record";
import { evolve } from "effect/Struct";
import { atom } from "recoil";

import type {
  IJsonArea,
  IJsonHomeland,
  IJsonMainState,
  IJsonSeaZone,
  IJsonStock,
  IJsonWorldState,
} from "../misc/jsonType";
import type { LogMessage } from "../misc/logMessage";
import {
  defaultAreaPosition,
  defaultSeaPosition,
  type Position,
} from "../misc/position";
import {
  worldStateValidator,
  worldStateValidatorOnTurnEnd,
} from "../misc/validator";
import type { Area } from "../models/area";
import type { Homeland } from "../models/homeland";
import type { SeaZone } from "../models/sea";
import type { Stock } from "../models/stock";
import type { AreaName, SeaName } from "../models/types";
import type { WorldModifier, WorldState } from "../models/worldState";
import { initTurn, startWorldState } from "./startpos";

const areaFromJson = (json: IJsonArea): Area => ({
  ...json,
  markers: HashMap.fromIterable(json.markers),
  changedMarkers: HashMap.fromIterable(json.changedMarkers),
  units: HashMap.fromIterable(json.units),
});
const seaZoneFromJson = (json: IJsonSeaZone): SeaZone => ({
  merchants: HashSet.fromIterable(json.merchants),
  units: HashMap.fromIterable(json.units),
});
const homelandFromJson = (json: IJsonHomeland): Homeland => ({
  ...json,
  units: HashMap.fromIterable(json.units),
});
const stockFromJson = (json: IJsonStock): Stock => ({
  ...json,
});

const worldStateFromJson = (json: IJsonWorldState): WorldState => ({
  areas: map(json.areas, areaFromJson),
  seaZones: map(json.seaZones, seaZoneFromJson),
  homelands: map(json.homelands, homelandFromJson),
  stocks: map(json.stocks, stockFromJson),
});

const mainStateFromJson = (json: IJsonMainState): MainState => ({
  ...json,
  worldState: worldStateFromJson(json.worldState),
  checkpoints: mapA(worldStateFromJson)(json.checkpoints),
  // TODO: snapshot無しでいいのか？
  snapshots: [],
  position: {
    areaPosition: defaultAreaPosition,
    seaPosition: defaultSeaPosition,
  },
  message: Option.fromNullable(json.message),
});

export const mainSate = atom<MainState>({
  key: "mainState",
  default: {
    worldState: startWorldState,
    checkpoints: [],
    snapshots: [],
    position: {
      areaPosition: defaultAreaPosition,
      seaPosition: defaultSeaPosition,
    },
    purchase: false,
    message: none(),
  },
});

export interface Snapshot {
  message: string;
  worldState: WorldState;
  checkpoints: WorldState[];
}
interface PlacePosition {
  areaPosition: typeof defaultAreaPosition;
  seaPosition: typeof defaultSeaPosition;
}
export interface MainState {
  worldState: WorldState;
  checkpoints: WorldState[];
  snapshots: Snapshot[];
  position: PlacePosition;
  purchase: boolean;
  message: Option.Option<LogMessage>;
}

type MainStateModifier = (m: MainState) => MainState;

export const doLoadData = (
  json: Either.Either<string, Error>,
): MainStateModifier =>
  pipe(
    json,
    Either.map((json: string) =>
      Schema.decodeUnknownSync(Schema.parseJson())(json),
    ),
    Either.match({
      onLeft: (l: unknown) =>
        evolve({
          message: constant(
            some<LogMessage>({
              body: l instanceof Error ? l.message : "unknown",
              level: "error",
            }),
          ),
        }),
      onRight: (json) =>
        constant({
          ...mainStateFromJson(json as IJsonMainState), // FIXME: schema
          message: some<LogMessage>({
            body: "読み込みました",
            level: "info",
          }),
        }),
    }),
  );

export const doTurnEnd =
  () =>
  (m: MainState): MainState => {
    const { checkpoints } = m;
    const body = `${1880 + 4 * checkpoints.length}年(ターン${
      checkpoints.length + 1
    })が終了`;
    return pipe(
      right(m.worldState),
      initTurn,
      flatMap(worldStateValidatorOnTurnEnd),
      Either.match({
        onLeft: (l: Error): MainState =>
          evolve(m, {
            message: constant(
              some({ body: l.message, level: "error" as const }),
            ),
          }),
        onRight: (w: WorldState): MainState =>
          evolve(m, {
            worldState: constant(w),
            checkpoints: (e) => append(w)(e),
            message: constant(some({ body, level: "info" as const })),
            snapshots: (e) =>
              append({ checkpoints, worldState: w, message: body })(e),
          }),
      }),
    );
  };

export const doSnackbarFinish = (message: LogMessage): MainStateModifier =>
  evolve({ message: constant(some(message)) });

export const doUndo =
  () =>
  (m: MainState): MainState =>
    matchRight(m.snapshots, {
      onEmpty: (): MainState =>
        evolve(m, {
          message: constant(
            some({ body: "snapshot is empty", level: "error" as const }),
          ),
        }),
      onNonEmpty: (left, snapshot): MainState =>
        evolve(m, {
          message: constant(
            some({ body: `Undo: ${snapshot.message}`, level: "info" as const }),
          ),
          snapshots: constant(left),
          worldState: constant(snapshot.worldState),
          checkpoints: constant(snapshot.checkpoints),
        }),
    });

export const doModifyAreaPosition = ({
  areaName,
  position,
}: {
  areaName: AreaName;
  position: Position;
}): MainStateModifier =>
  evolve({
    position: (p) => ({
      ...p,
      areaPosition: { ...p.areaPosition, [areaName]: position },
    }),
  });
export const doModifySeaPosition = ({
  seaName,
  position,
}: {
  seaName: SeaName;
  position: Position;
}): MainStateModifier =>
  evolve({
    position: (p) => ({
      ...p,
      seaPosition: { ...p.seaPosition, [seaName]: position },
    }),
  });

export const doModifyWorldFinish =
  ({ modifier, message }: { modifier: WorldModifier; message: string }) =>
  (m: MainState): MainState =>
    pipe(
      m.worldState,
      modifier,
      flatMap(worldStateValidator),
      Either.match({
        onLeft: (l: Error): MainState =>
          evolve(m, {
            message: constant(
              some({ body: l.message, level: "error" as const }),
            ),
          }),
        onRight: (w: WorldState): MainState =>
          evolve(m, {
            worldState: constant(w),
            message: constant(some({ body: message, level: "info" as const })),
            snapshots: (snapshots) =>
              append(snapshots, {
                worldState: w,
                message,
                checkpoints: m.checkpoints,
              }),
          }),
      }),
    );
