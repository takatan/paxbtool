import { constant, pipe } from "effect/Function";
import * as HM from "effect/HashMap";
import * as HS from "effect/HashSet";
import * as RR from "effect/Record";

import {
  areaDataRecord,
  powerDataRecord,
  seaDataRecord,
} from "../misc/database";
import type { Area } from "../models/area";
import type { Homeland } from "../models/homeland";
import type { SeaZone } from "../models/sea";
import type { Stock } from "../models/stock";
import type { PowerName } from "../models/types";
import type { WorldState } from "../models/worldState";

export const defaultArea: Area = {
  markers: HM.empty(),
  changedMarkers: HM.empty(),
  units: HM.empty(),
  unrest: false,
  canal: false,
};

export const defaultStock: Stock = {
  merchant: 0,
  units: {
    10: 0,
    5: 0,
    3: 0,
    1: 0,
  },
  round: 0,
  hex: 0,
  dominion: 0, // Dominion/Possession
  state: 0, // State/Possession
};

export const defaultSeaZone: SeaZone = {
  merchants: HS.empty(),
  units: HM.empty(),
};

export const defaultHomeland: Homeland = {
  treasuryEvents: [],
  merchant: 0,
  units: HM.empty(),
  vpEvents: [],
};

const defaultStockRecord: Record<PowerName, Stock> = {
  "Great Britain": {
    units: { 10: 15, 5: 0, 3: 14, 1: 32 },
    dominion: 4,
    state: 0,
    hex: 40,
    round: 20,
    merchant: 10,
  },
  France: {
    units: { 10: 10, 5: 0, 3: 10, 1: 20 },
    dominion: 0,
    state: 0,
    hex: 20,
    round: 18,
    merchant: 7,
  },
  Germany: {
    units: { 10: 8, 5: 0, 3: 10, 1: 15 },
    dominion: 0,
    state: 0,
    hex: 15,
    round: 18,
    merchant: 7,
  },
  "United States": {
    units: { 10: 8, 5: 0, 3: 10, 1: 12 },
    dominion: 0,
    state: 4,
    hex: 12,
    round: 15,
    merchant: 8,
  },
  Japan: {
    units: { 10: 8, 5: 0, 3: 8, 1: 10 },
    dominion: 0,
    state: 0,
    hex: 10,
    round: 12,
    merchant: 5,
  },
  Italy: {
    units: { 10: 8, 5: 0, 3: 8, 1: 8 },
    dominion: 0,
    state: 0,
    hex: 8,
    round: 10,
    merchant: 4,
  },
  Russia: {
    units: { 10: 8, 5: 0, 3: 8, 1: 7 },
    dominion: 0,
    state: 0,
    hex: 7,
    round: 10,
    merchant: 3,
  },
  Spain: {
    units: { 10: 0, 5: 0, 3: 5, 1: 6 },
    dominion: 0,
    state: 0,
    hex: 6,
    round: 2,
    merchant: 4 + 2, // errata
  },
  Netherlands: {
    units: { 10: 0, 5: 0, 3: 3, 1: 4 },
    dominion: 0,
    state: 0,
    hex: 4,
    round: 2,
    merchant: 4,
  },
  "Austria-Hungary": {
    units: { 10: 4, 5: 0, 3: 1, 1: 4 },
    dominion: 0,
    state: 0,
    hex: 4,
    round: 5,
    merchant: 0,
  },
  Portugal: {
    units: { 10: 0, 5: 0, 3: 1, 1: 5 },
    dominion: 0,
    state: 0,
    hex: 3,
    round: 2,
    merchant: 3,
  },
  Belgium: {
    units: { 10: 0, 5: 0, 3: 2, 1: 4 },
    dominion: 0,
    state: 0,
    hex: 3,
    round: 3,
    merchant: 2,
  },
  "Chinese Empire": {
    units: { 10: 0, 5: 6, 3: 0, 1: 0 },
    dominion: 0,
    state: 0,
    hex: 0,
    round: 0,
    merchant: 0,
  },
  "Ottoman Empire": {
    units: { 10: 0, 5: 6, 3: 0, 1: 0 },
    dominion: 0,
    state: 0,
    hex: 0,
    round: 0,
    merchant: 0,
  },
};

export const emptyWorldState: WorldState = {
  areas: pipe(areaDataRecord, RR.map(constant(defaultArea))),
  seaZones: pipe(seaDataRecord, RR.map(constant(defaultSeaZone))),
  homelands: pipe(powerDataRecord, RR.map(constant(defaultHomeland))),
  stocks: pipe(defaultStockRecord, RR.map(constant(defaultStock))),
};

export const initWorldState = (): WorldState => ({
  areas: pipe(areaDataRecord, RR.map(constant(defaultArea))),
  seaZones: pipe(seaDataRecord, RR.map(constant(defaultSeaZone))),
  homelands: pipe(powerDataRecord, RR.map(constant(defaultHomeland))),
  stocks: defaultStockRecord,
});
