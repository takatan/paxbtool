import "./css/App.scss";
import "./css/position.scss";

import type * as React from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { RecoilRoot } from "recoil";

import PaxBritannicaApp from "./components/PaxBritannica";

export const App: React.FC = () => (
    <RecoilRoot>
      <DndProvider backend={HTML5Backend}>
        <PaxBritannicaApp />
      </DndProvider>
    </RecoilRoot>
  );
